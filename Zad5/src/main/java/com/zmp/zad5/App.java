package com.zmp.zad5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class App {
    // PKT 1
    static void printElementsWithAge(List<Student> students, int age) {
        students.stream().filter((s) -> s.getAge() == age).forEach((s) -> System.out.println(s));
    }

    static List<Student> getElementsWithAge(List<Student> students, int age) {
        return students.stream().filter((s) -> s.getAge() == age).collect(Collectors.toList());
    }

    // PKT 2
    static void printSchoolOfClasses(List<SchoolClass> classes) {
        classes.stream().map((c) -> c.getSchool().getSchoolPatron()).forEach((n) -> System.out.println(n));
    }

    static List<String> getSchoolOfClasses(List<SchoolClass> classes) {
        return classes.stream().map((c) -> c.getSchool().getSchoolPatron()).collect(Collectors.toList());
    }

    // PKT 3
    static void printClassesWithMinAndMaxStudents(List<SchoolClass> classes) {
        Optional<SchoolClass> max = classes.stream().max(Comparator.comparing(SchoolClass::getClassCapacity));

        Optional<SchoolClass> min = classes.stream().min(Comparator.comparing(SchoolClass::getClassCapacity));
        if (max.isPresent()) {
            classes.stream().filter(sc -> sc.getClassCapacity() == max.get().getClassCapacity())
                    .forEach((s) -> System.out.println(s));
        }
        if (min.isPresent()) {
            classes.stream().filter(sc -> sc.getClassCapacity() == min.get().getClassCapacity())
                    .forEach((s) -> System.out.println(s));
        }
    }

    static List<SchoolClass> getClassesWithMinAndMaxStudents(List<SchoolClass> classes) {
        Optional<SchoolClass> opt = classes.stream().max(Comparator.comparing(SchoolClass::getClassCapacity));
        List<SchoolClass> result = new ArrayList<>();
        if (opt.isPresent()) {
            result.addAll(classes.stream().filter(sc -> sc.getClassCapacity() == opt.get().getClassCapacity())
                    .collect(Collectors.toList()));
        }
        if (opt.isPresent()) {
            result.addAll(classes.stream().filter(sc -> sc.getClassCapacity() == opt.get().getClassCapacity())
                    .collect(Collectors.toList()));
        }
        return result;
    }

    // PKT 4
    static void printAvarageAgeByGender(List<Student> students) {
        students.stream().collect(Collectors.groupingBy(Student::getGender, Collectors.averagingInt(Student::getAge)))
                .forEach((a, b) -> {
                    System.out.println(a + " " + b);
                });
    }

    static Map<Character, Double> getAvarageAgeByGender(List<Student> students) {
        return students.stream()
                .collect(Collectors.groupingBy(Student::getGender, Collectors.averagingInt(Student::getAge)));
    }

    public static void main(String[] args) {
        int schoolsAmount = 20;
        int schoolClassesAmount = 30;
        List<School> schools = new ArrayList<>();
        List<SchoolClass> schoolClasses = new ArrayList<>();
        List<Student> students = new ArrayList<>();
        for (int i = 0; i < schoolsAmount; i++) {
            School school = FakerBuild.buildRandomSchool();
            for (int j = 0; j < schoolClassesAmount; j++) {
                SchoolClass schoolClass = FakerBuild.buildRandomClass(school);
                for (int k = 0; k < schoolClass.getClassCapacity(); k++) {
                    students.add(FakerBuild.buildRandomStudent(schoolClass));
                }
                schoolClasses.add(schoolClass);
            }
            schools.add(school);
        }

        printElementsWithAge(students, 10);
        printSchoolOfClasses(schoolClasses);
        printClassesWithMinAndMaxStudents(schoolClasses);
        printAvarageAgeByGender(students);

        for (Student s : getElementsWithAge(students, 10)) {
            System.out.println(s);
        }
        for (String sc : getSchoolOfClasses(schoolClasses)) {
            System.out.println(sc);
        }

        for (SchoolClass sc : getClassesWithMinAndMaxStudents(schoolClasses)) {
            System.out.println(sc);
        }

        for (Map.Entry<Character, Double> i : getAvarageAgeByGender(students).entrySet()) {
            System.out.println(i.getKey() + " " + i.getValue());
        }

        Scanner in = new Scanner(System.in);

        int option = -1;
        do {

            System.out.println("1. Wyświetl uczniów w wieku 10 lat (filter)");
            System.out.println("2. Wyświetl szkoły wszystkich klas (map)");
            System.out.println("3. Wyświetl klasy z największą i najmniejszą dopuszczalna liczbą uczniów (min, max)");
            System.out.println("4. Wyświetl średnią wieku wsród uczniów w zależności od płci");
            try {
                option = in.nextInt();

                switch (option) {
                case 1:
                    printElementsWithAge(students, 10);
                    break;
                case 2:
                    printSchoolOfClasses(schoolClasses);
                    break;
                case 3:
                    printClassesWithMinAndMaxStudents(schoolClasses);
                    break;
                case 4:
                    printAvarageAgeByGender(students);
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Podałeś liczbę w złym formacie");
                in.nextLine();
            }
        } while (option != 0);
        in.close();
    }
}
