package com.zmp.zad5;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

import org.apache.commons.lang3.SerializationUtils;

public class Student implements Cloneable, Serializable, IDeepCopy<Student>, IByteDeepCopy<Student> {

    private static final long serialVersionUID = 8850045263120012153L;
    private String firstname;
    private String lastname;
    private int age;
    private int[] PESEL;
    private char gender;
    private SchoolClass schoolClass;

    public Student(String firstname, String lastname, int age, int[] PESEL, char gender) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.PESEL = PESEL;
        this.gender = gender;
    }

    public Student(String firstname, String lastname, int age, int[] PESEL, char gender, SchoolClass schoolClass) {
        this(firstname, lastname, age, PESEL, gender);
        this.schoolClass = schoolClass;
    }

    public Student(Student other) {
        this.firstname = other.firstname;
        this.lastname = other.lastname;
        this.age = other.age;
        this.gender = other.gender;
        Arrays.copyOf(other.PESEL, other.PESEL.length);
    }

    @Override
    public Student clone() {
        try {
            Student student = (Student) super.clone();
            student.PESEL = this.PESEL.clone();
            return student;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @return the pESEL
     */
    public int[] getPESEL() {
        return PESEL;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @param pESEL the pESEL to set
     */
    public void setPESEL(int[] pESEL) {
        PESEL = pESEL;
    }

    @Override
    public Student deepCopy() {
        return SerializationUtils.clone(this);
    }

    @Override
    public Student byteDeepCopy() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStrm;
        try {
            outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(this);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            return (Student) objInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "FirstName = " + this.firstname + "\nLastName = " + this.lastname + "\nAge = " + this.age + "\nPESEL = "
                + Arrays.toString(this.PESEL);
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    /**
     * @param schoolClass the schoolClass to set
     */
    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

    /**
     * @return the schoolClass
     */
    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

}