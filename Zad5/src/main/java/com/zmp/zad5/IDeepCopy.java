package com.zmp.zad5;

public interface IDeepCopy<T> {
    T deepCopy();
}