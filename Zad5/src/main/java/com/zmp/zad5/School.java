package com.zmp.zad5;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

public class School implements Cloneable, Serializable, IDeepCopy<School>, IByteDeepCopy<School> {

    private static final long serialVersionUID = -2775240257977918789L;

    static public enum SchoolType {
        PRIMARY, JUNIOR_HIGH_SCHOOL, HIGH_SCHOOL;
        private static Random random = new Random();

        public static SchoolType getRandomSchoolType() {
            switch (random.nextInt(3)) {
            case 0:
                return PRIMARY;
            case 1:
                return JUNIOR_HIGH_SCHOOL;
            case 2:
                return HIGH_SCHOOL;
            }
            return null;
        }
    };

    private String city;
    private String street;
    private int number;
    private String schoolPatron;
    private SchoolType schoolType;
    private List<SchoolClass> schoolClasses;

    public School(String city, String street, int number, String schoolPatron, SchoolType schoolType) {
        this.city = city;
        this.street = street;
        this.number = number;
        this.schoolPatron = schoolPatron;
        this.schoolType = schoolType;
        this.schoolClasses = new ArrayList<>();
    }

    public School(School other) {
        this.schoolClasses = new ArrayList<>(other.schoolClasses.size());
        for (SchoolClass clz : other.schoolClasses) {
            this.schoolClasses.add(new SchoolClass(clz));
        }
    }

    @Override
    public School clone() {
        try {
            School school = (School) super.clone();
            school.schoolClasses = new ArrayList<>(this.schoolClasses.size());
            for (SchoolClass sc : this.schoolClasses) {
                school.schoolClasses.add(sc.clone());
            }
            return school;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @param schoolClasses the schoolClasses to set
     */
    public void addSchoolClasses(List<SchoolClass> schoolClasses) {
        this.schoolClasses.addAll(schoolClasses);
    }

    /**
     * @param schoolClasses the schoolClasses to set
     */
    public void addSchoolClasse(SchoolClass schoolClasse) {
        this.schoolClasses.add(schoolClasse);
    }

    /**
     * @param schoolPatron the schoolPatron to set
     */
    public void setSchoolPatron(String schoolPatron) {
        this.schoolPatron = schoolPatron;
    }

    /**
     * @param schoolType the schoolType to set
     */
    public void setSchoolType(SchoolType schoolType) {
        this.schoolType = schoolType;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @return the schoolClasses
     */
    public List<SchoolClass> getSchoolClasses() {
        return schoolClasses;
    }

    /**
     * @return the schoolPatron
     */
    public String getSchoolPatron() {
        return schoolPatron;
    }

    /**
     * @return the schoolType
     */
    public SchoolType getSchoolType() {
        return schoolType;
    }

    @Override
    public School deepCopy() {
        return SerializationUtils.clone(this);
    }

    @Override
    public School byteDeepCopy() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStrm;
        try {
            outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(this);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            return (School) objInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}