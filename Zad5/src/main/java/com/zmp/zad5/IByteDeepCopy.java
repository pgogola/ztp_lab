package com.zmp.zad5;

interface IByteDeepCopy<T> {
    T byteDeepCopy();
}