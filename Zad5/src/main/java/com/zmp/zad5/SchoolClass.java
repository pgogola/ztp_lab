package com.zmp.zad5;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

public class SchoolClass implements Cloneable, Serializable, IDeepCopy<SchoolClass>, IByteDeepCopy<SchoolClass> {

    private static final long serialVersionUID = 8442539472969873627L;

    static public enum ClassType {
        MATH, PHISICAL, SPORT, HUMANITIES;

        private static Random random = new Random();

        public static ClassType getRandomSchoolClassType() {
            switch (random.nextInt(4)) {
            case 0:
                return MATH;
            case 1:
                return PHISICAL;
            case 2:
                return SPORT;
            case 3:
                return HUMANITIES;
            }
            return null;
        }
    };

    private int classLevel;
    private final char classNumber;
    private final int classCapacity;
    private ClassType classType;
    private List<Student> students;
    private School school;

    public SchoolClass(int classLevel, char classNumber, int classCapacity, ClassType classType) {
        this.classLevel = classLevel;
        this.classNumber = classNumber;
        this.classCapacity = classCapacity;
        this.classType = classType;
        this.students = new ArrayList<>(classCapacity);
    }

    public SchoolClass(int classLevel, char classNumber, int classCapacity, ClassType classType, School school) {
        this(classLevel, classNumber, classCapacity, classType);
        this.school = school;
    }

    public SchoolClass(SchoolClass other) {
        this.classLevel = other.classLevel;
        this.classNumber = other.classNumber;
        this.classCapacity = other.classCapacity;
        this.classType = other.classType;
        this.students = new ArrayList<>(other.classCapacity);
        for (Student stud : other.students) {
            this.students.add(new Student(stud));
        }
    }

    @Override
    public SchoolClass clone() {
        try {
            SchoolClass schoolClass = (SchoolClass) super.clone();
            schoolClass.students = new ArrayList<>(this.students.size());
            for (Student s : this.students) {
                schoolClass.students.add(s.clone());
            }
            return schoolClass;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return the students
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * @return the classCapacity
     */
    public int getClassCapacity() {
        return classCapacity;
    }

    /**
     * @return the classLevel
     */
    public int getClassLevel() {
        return classLevel;
    }

    /**
     * @return the classNumber
     */
    public char getClassNumber() {
        return classNumber;
    }

    public boolean addStudent(Student student) {
        return (this.students.size() < this.classCapacity) && this.students.add(student);
    }

    public boolean addStudents(List<Student> students) {
        return (this.students.size() + students.size() < this.classCapacity) && this.students.addAll(students);
    }

    public Student getStudentByNumberInClass(int index) {
        try {
            return students.get(index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @return the classType
     */
    public ClassType getClassType() {
        return classType;
    }

    public Student removeStudentByNumberInClass(int index) {
        try {
            return students.remove(index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * @param classLevel the classLevel to set
     */
    public void setClassLevel(int classLevel) {
        this.classLevel = classLevel;
    }

    /**
     * @param classType the classType to set
     */
    public void setClassType(ClassType classType) {
        this.classType = classType;
    }

    @Override
    public SchoolClass deepCopy() {
        return SerializationUtils.clone(this);
    }

    @Override
    public SchoolClass byteDeepCopy() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStrm;
        try {
            outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(this);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            return (SchoolClass) objInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the school
     */
    public School getSchool() {
        return school;
    }

    /**
     * @param school the school to set
     */
    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public String toString() {
        return "Class number = " + this.classNumber + "\nClass capacity = " + this.classCapacity + "\nClass type = "
                + this.classType + "\nStudents amount = " + this.students.size() + "\nSchool name = "
                + this.school.getSchoolPatron();
    }
}