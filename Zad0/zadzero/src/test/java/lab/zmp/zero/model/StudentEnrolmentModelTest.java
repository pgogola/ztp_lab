package lab.zmp.zero.model;

import org.junit.Test;

public class StudentEnrolmentModelTest {

    @Test
    public void When_StudentIndexIsCorrect_Expect_NoThrow() {
        Student student1 = new Student("n1", "s1", "123457");
    }


    @Test(expected = IllegalArgumentException.class)
    public void When_StudentIndexIsTooLong_Expect_IllegalArgumentException() {
        Student student1 = new Student("n1", "s1", "1234567");
    }

    @Test(expected = IllegalArgumentException.class)
    public void When_StudentIndexIsShort_Expect_IllegalArgumentException() {
        Student student2 = new Student("n1", "s1", "12355");
    }

    @Test(expected = IllegalArgumentException.class)
    public void When_StudentIndexHasNonDigitCharacters_Expect_IllegalArgumentThrow() {
        Student student1 = new Student("n1", "s1", "12_4*a");
    }

//    @Test
//    public void When_StudentIndexArrayIsCorrect_Expect_NoThrow() {
//        Byte[] index = {1, 2, 3, 5, 5, 6};
//        Student studentModel1 = new Student("n1", "s1", index);
//    }
//
//
//    @Test(expected = IllegalArgumentException.class)
//    public void When_StudentIndexArrayIsTooLong_Expect_IllegalArgumentException() {
//        Byte[] index = {1, 2, 3, 5, 5, 7, 8};
//        Student studentModel1 = new Student("n1", "s1", index);
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void When_StudentIndexArrayIsShort_Expect_IllegalArgumentException() {
//        Byte[] index = {1, 2, 3, 5, 5};
//        Student studentModel2 = new Student("n1", "s1", index);
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void When_StudentIndexArrayHasNonDigitCharacters_Expect_IllegalArgumentThrow() {
//        Byte[] index = {1, 2, '_', 4, '*', 'a'};
//        Student studentModel1 = new Student("n1", "s1", index);
//    }
}
