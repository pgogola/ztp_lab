package lab.zmp.zero.model;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import org.hamcrest.Matchers;

public class CourseEnrolmentModelTest {

    private Course course;
    private Course.Group group;
    private final String COURSE_NAME = "COURSE_NAME";
    private final String COURSE_CODE = "COURSE_CODE";
    private final String GROUP_CODE = "GROUP_CODE";

    @Before
    public void setUp() {

        this.course = new Course(this.COURSE_NAME, this.COURSE_CODE);
        this.group = this.course.addCourseGroup(GROUP_CODE, 10);
    }

    //GROUPS_TEST
    @Test
    public void When_WantToAddStudentWithTheSameIndex_Expect_ReturnSTUDENT_IN_GROUP() {
        assertThat(group.addStudent(new Student("n1", "s1", "123456")), Matchers.equalTo(Course.Group.GROUP_ERROR.OK));
        assertThat(group.addStudent(new Student("n2", "s2", "123456")), Matchers.equalTo(Course.Group.GROUP_ERROR.STUDENT_IN_GROUP));
    }

    @Test
    public void When_WantToSetGroupCapacityLowerThanCurrentAmountInStudents_Expect_ReturnFalse() {
        group.addStudent(new Student("n1", "s1", "123445"));
        group.addStudent(new Student("n2", "s2", "223456"));
        group.addStudent(new Student("n3", "s3", "323456"));
        group.addStudent(new Student("n4", "s4", "423456"));
        group.addStudent(new Student("n5", "s5", "512345"));
        group.addStudent(new Student("n6", "s6", "612345"));
        group.addStudent(new Student("n7", "s7", "712345"));
        assertFalse(group.setGroupCapacity(group.getSignedUpStudentsAmount() - 1));
    }

    @Test
    public void When_GroupIsFull_Expect_ReturnFULL_GROUP() {
        group.addStudent(new Student("n1", "s1", "123456"));
        group.addStudent(new Student("n2", "s2", "221345"));
        group.setGroupCapacity(group.getSignedUpStudentsAmount());
        Student student = mock(Student.class);
        assertThat(this.course.signUpStudentToCourse(this.group, student),
                Matchers.equalTo(Course.COURSE_ERROR.FULL_GROUP));
    }

    //END_OF_GROUP_TEST

    //COURSE_TEST
    @Test(expected = IllegalArgumentException.class)
    public void When_CourseNameIsEmpty_Expect_IllegalArgumentExceptionIsThrown() {
        final String code = "SOME_CODE";
        new Course("", code);
    }

    @Test(expected = IllegalArgumentException.class)
    public void When_CourseCodeIsEmpty_IllegalArgumentExceptionIsThrown() {
        final String name = "SOME_NAME";
        new Course(name, "");
    }


    @Test(expected = IllegalArgumentException.class)
    public void When_NewGroupIsCreatedAndCapacityIsLowerThanOne_Expect_IllegalArgumentException() {
        final String GROUP_CODE_TEST = "GROUP_CODE_TEST";
        final Integer GROUP_CAPACITY = -1;
        this.course.addCourseGroup(GROUP_CODE_TEST, GROUP_CAPACITY);
    }

    @Test
    public void When_NewGroupIsCreated_Expect_GroupIsCreatedAndExistsInCourse() {
        int prevCourseGroupsAmount = this.course.getGroups().size();
        final String GROUP_CODE_TEST = "GROUP_CODE_TEST";
        final Integer GROUP_CAPACITY = 15;
        Course.Group addedGroup = this.course.addCourseGroup(GROUP_CODE_TEST, GROUP_CAPACITY);
        assertThat(addedGroup, Matchers.notNullValue());
        assertThat(this.course.getGroups().size(), Matchers.is(prevCourseGroupsAmount + 1));
//        assertThat(this.course.getGroups(), IsMapContaining.hasKey(GROUP_CODE_TEST));
        assertThat(addedGroup.getCode(), Matchers.equalTo(GROUP_CODE_TEST));
    }

    @Test
    public void When_NewGroupHasTheSameCodeAsAnyGroupInCourse_Expect_GroupIsNotCreatedReturnNull() {
        final String GROUP_CODE_TEST = "GROUP_CODE_TEST";
        final Integer GROUP_CAPACITY = 15;
        this.course.addCourseGroup(GROUP_CODE_TEST, GROUP_CAPACITY);
        int prevCourseGroupsAmount = this.course.getGroups().size();
        Course.Group addedGroup = this.course.addCourseGroup(GROUP_CODE_TEST, GROUP_CAPACITY);
        assertThat(addedGroup, Matchers.nullValue());
        assertThat(this.course.getGroups().size(), Matchers.is(prevCourseGroupsAmount));
//        assertThat(this.course.getGroups(), IsMapContaining.hasKey(GROUP_CODE_TEST));
    }

    @Test
    public void When_StudentHasNotBeenSignedUpToCourse_Expect_AllowSignUp() {
        Student student = mock(Student.class);
        when(student.getIndex()).thenReturn("123456");
        assertThat(this.course.signUpStudentToCourse(this.group, student),
                Matchers.equalTo(Course.COURSE_ERROR.OK));
    }
    //END_OF_COURSE_TESTS
}