package lab.zmp.zero.model;

import javax.jdo.annotations.*;
import java.util.*;

@PersistenceCapable
public class Course {

    public enum COURSE_ERROR {
        OK,
        FULL_GROUP,
        STUDENT_IN_COURSE
    }

    // GROUP CLASS
    @PersistenceCapable
    static public class Group implements Comparable<Group> {
        @Override
        public int compareTo(Group group) {
            return this.getCode().compareTo(group.getCode());
        }

        public enum GROUP_ERROR {
            OK,
            FULL_GROUP,
            STUDENT_IN_GROUP
        }

        @PrimaryKey
        private String code;

//        @Element(column = "STUDENT_ID")
        @Join
        private Set<Student> studentsList;

        private Integer groupCapacity;


        public Group() {

        }

        public Group(String code, Integer groupCapacity) {
            this.code = code;
            this.setGroupCapacity(groupCapacity);
            this.studentsList = new HashSet<>();
        }

        /**
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * @return the groupCapacity
         */
        public Integer getGroupCapacity() {
            return groupCapacity;
        }

        /**
         * @param groupCapacity the groupCapacity to set
         */
        public boolean setGroupCapacity(Integer groupCapacity) {
            if (groupCapacity < 1) {
                throw new IllegalArgumentException("Group capacity cannot be lower than 1");
            }
            if (null != this.groupCapacity && groupCapacity < studentsList.size()) {
                return false;
            }

            this.groupCapacity = groupCapacity;
            return true;
        }

        /**
         * @return the studentsList
         */
        public Set<Student> getStudentsList() {
            return studentsList;
        }

        public GROUP_ERROR addStudent(Student student) {
            if (this.groupCapacity <= this.studentsList.size()) {
                return GROUP_ERROR.FULL_GROUP;
            }
            if (this.studentsList.contains(student)) {
                return GROUP_ERROR.STUDENT_IN_GROUP;
            }
            this.studentsList.add(student);
            return GROUP_ERROR.OK;
        }

        public int getSignedUpStudentsAmount() {
            return this.studentsList.size();
        }
    }
    //ENDOF_GROUPCLASS


    public Course() {

    }

    public Course(String name, String code) {
        assert name != null : "Course name is null";
        assert code != null : "Course code is null";

        if (name.isEmpty() || code.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.code = code;
        this.groups = new TreeSet<>();
        this.studentsIndexes = new TreeSet<>();
    }

    public Group addCourseGroup(String groupCode, Integer groupCapacity) {
        Group newGroup = new Group(groupCode, groupCapacity);
        if (this.groups.contains(newGroup)) {
            return null;
        }
        this.groups.add(newGroup);
        return newGroup;
    }

    public COURSE_ERROR signUpStudentToCourse(Group group, Student student) {
        if (studentsIndexes.contains(student.getIndex())) {
            return COURSE_ERROR.STUDENT_IN_COURSE;
        }
        switch (group.addStudent(student)) {
            case OK:
                studentsIndexes.add(student.getIndex());
                return COURSE_ERROR.OK;
            case FULL_GROUP:
                return COURSE_ERROR.FULL_GROUP;
            case STUDENT_IN_GROUP:
                return COURSE_ERROR.STUDENT_IN_COURSE;
        }
        return COURSE_ERROR.OK;
    }

    public void removeStudentFromCourse(Student student) {
        for (Group g : groups) {
            if (g.getStudentsList().remove(student)) {
                studentsIndexes.remove(student.getIndex());
            }
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the groups
     */
    public Set<Group> getGroups() {
        return groups;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<String> getStudentsIndexes() {
        return studentsIndexes;
    }

    public void setStudentsIndexes(Set<String> studentsIndexes) {
        this.studentsIndexes = studentsIndexes;
    }

    @PrimaryKey
    private String code;

    private String name;

//    @Element(column = "GROUP_ID")

    private Set<Group> groups;
    private Set<String> studentsIndexes;
}