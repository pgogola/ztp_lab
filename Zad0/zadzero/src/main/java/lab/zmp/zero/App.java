package lab.zmp.zero;

import lab.zmp.zero.controller.Controller;
import lab.zmp.zero.model.EnrolmentModel;
import lab.zmp.zero.view.IView;
import lab.zmp.zero.view.TestView;
import lab.zmp.zero.view.View;


public class App 
{
    public static void main(String[] args) {
        IView view = new View();
        EnrolmentModel model = new EnrolmentModel();
        Controller controller = new Controller(view, model);

        controller.run();
    }
}
