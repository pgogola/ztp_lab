package lab.zmp.zero.repository;

import lab.zmp.zero.model.Student;

import javax.jdo.*;
import java.util.List;

public class StudentRepository implements IRepository<Student, String> {
    private PersistenceManagerFactory pmf;
    private PersistenceManager pm;

    public StudentRepository(PersistenceManagerFactory pmf, PersistenceManager pm) {
        this.pmf = pmf;
        this.pm = pm;
    }

    @Override
    public Student getById(String id) {
        Transaction tx = pm.currentTransaction();
        Student student = null;
        try {
            tx.begin();
            student = pm.getObjectById(Student.class, id);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return student;
    }

    @Override
    public List<Student> listAll() {
        Transaction tx = pm.currentTransaction();
        List<Student> students = null;
        try {
            tx.begin();
            Query q = pm.newQuery("SELECT FROM " + Student.class.getName());
            students = (List<Student>) q.execute();
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return students;
    }

    @Override
    public Student add(Student entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return entity;
    }

    @Override
    public void remove(Student entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.deletePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    @Override
    public void update(Student entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }
}
