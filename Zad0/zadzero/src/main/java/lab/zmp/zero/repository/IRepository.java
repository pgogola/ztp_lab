package lab.zmp.zero.repository;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import java.util.List;

public interface IRepository<Type, IdType> {
    Type getById(IdType id);
    List<Type> listAll();
    Type add(Type entity);
    void remove(Type entity);
    void update(Type entity);
}
