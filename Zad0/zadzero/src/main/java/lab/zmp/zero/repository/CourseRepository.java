package lab.zmp.zero.repository;

import lab.zmp.zero.model.Course;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;
import java.util.List;

public class CourseRepository implements IRepository<Course, String> {

    private PersistenceManagerFactory pmf;
    private PersistenceManager pm;

    public CourseRepository(PersistenceManagerFactory pmf, PersistenceManager pm) {
        this.pmf = pmf;
        this.pm = pm;
    }

    @Override
    public Course getById(String id) {
        Course course = null;
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            course = pm.getObjectById(Course.class, id);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return course;
    }

    @Override
    public List<Course> listAll() {
        Transaction tx = pm.currentTransaction();
        List<Course> courses = null;
        try {
            tx.begin();
            Query q = pm.newQuery("SELECT FROM " + Course.class.getName());
            courses = (List<Course>) q.execute();
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }

        return courses;
    }

    @Override
    public Course add(Course entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return entity;
    }

    @Override
    public void remove(Course entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.deletePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    @Override
    public void update(Course entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }
}
