package lab.zmp.zero.view;

import lab.zmp.zero.controller.IController;
import lab.zmp.zero.model.Course;
import lab.zmp.zero.model.Student;

import java.util.List;
import java.util.Set;

public interface IView {

    public boolean askUser();

    public void setControllerListener(IController controllerListener);

    public void listCourses(List<Course> courses);

    public void listGroupsInCourse(Set<Course.Group> groups);

    public void listStudents(List<Student> students);

    public void listStudentsInCourseGroup(Set<Student> students);

    public void showManual(List<String> manual);

    public void printMessage(String message);

}
