package lab.zmp.zero.view;

import lab.zmp.zero.controller.IController;
import lab.zmp.zero.model.Course;
import lab.zmp.zero.model.Student;

import java.util.*;

public class View implements IView {
    private IController controllerListener;
    private final static Scanner sc = new Scanner(System.in);

    public void setControllerListener(IController controllerListener) {
        this.controllerListener = controllerListener;
    }

    public boolean askUser() {
        System.out.print("~> ");
        String line = sc.nextLine();
        System.out.println(line);
        return controllerListener.viewHandler(line);
    }

    public void listCourses(List<Course> courses) {
        for (Course cm : courses) {
            System.out.println(cm.getName() + " " + cm.getCode() + " ");
        }
    }

    public void listGroupsInCourse(Set<Course.Group> groups) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Course.Group group = (Course.Group) it.next();
            System.out.println(group.getCode() + " " + group.getSignedUpStudentsAmount() + " " + group.getGroupCapacity());
        }
    }

    public void listStudents(List<Student> students) {
        for (Student sm : students) {
            System.out.println(sm.getName() + " " + sm.getSurname() + " " + sm.getIndex());
        }
    }

    public void listStudentsInCourseGroup(Set<Student> students) {
        Iterator it = students.iterator();
        while (it.hasNext()) {
            Student sm = (Student) it.next();
            System.out.println(sm.getName() + " " + sm.getSurname() + " " + sm.getIndex());
        }
    }

    public void showManual(List<String> manual) {
        for (String s : manual) {
            System.out.println(s);
        }
    }

    public void printMessage(String message) {
        if (null != message) {
            System.out.println(message);
        }
    }


}
