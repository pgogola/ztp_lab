package lab.zmp.zero.repository;

import lab.zmp.zero.model.Course;

import javax.jdo.*;
import java.util.List;

public class GroupRepository implements IRepository<Course.Group, String> {

    private PersistenceManagerFactory pmf;
    private PersistenceManager pm;

    public GroupRepository(PersistenceManagerFactory pmf, PersistenceManager pm) {
        this.pmf = pmf;
        this.pm = pm;
    }

    @Override
    public Course.Group getById(String id) {
        Transaction tx = pm.currentTransaction();
        Course.Group group = null;
        try {
            tx.begin();
            group = pm.getObjectById(Course.Group.class, id);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return group;
    }

    @Override
    public List<Course.Group> listAll() {
        Transaction tx = pm.currentTransaction();
        List<Course.Group> groups = null;
        try {
            tx.begin();
            Query q = pm.newQuery("SELECT FROM " + Course.Group.class.getName());
            groups = (List<Course.Group>) q.execute();
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return groups;
    }

    @Override
    public Course.Group add(Course.Group entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        return entity;
    }

    @Override
    public void remove(Course.Group entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.deletePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }

    @Override
    public void update(Course.Group entity) {
        Transaction tx = pm.currentTransaction();
        try {
            tx.begin();
            pm.makePersistent(entity);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }
}
