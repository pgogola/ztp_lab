package lab.zmp.zero.controller;

public interface IController {
    public boolean viewHandler(String command);
}
