package lab.zmp.zero.model;

import lab.zmp.zero.repository.CourseRepository;
import lab.zmp.zero.repository.GroupRepository;
import lab.zmp.zero.repository.StudentRepository;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import java.util.List;
import java.util.Set;

public class EnrolmentModel {

    private PersistenceManagerFactory pmf;
    private PersistenceManager pm;
    private CourseRepository courseRepository;
    private StudentRepository studentRepository;
    private GroupRepository groupRepository;

    public EnrolmentModel() {
        pmf = JDOHelper.getPersistenceManagerFactory("Tutorial");
        pm = pmf.getPersistenceManager();
        courseRepository = new CourseRepository(pmf, pm);
        studentRepository = new StudentRepository(pmf, pm);
        groupRepository = new GroupRepository(pmf, pm);
    }


    public void addStudent(String name, String surname, String index) {
        studentRepository.add(new Student(name, surname, index));
    }

    public Course.COURSE_ERROR addStudentToGroup(String studentIndex, String groupCode) {
        Course.Group group = groupRepository.getById(groupCode);
        List<Course> courses = courseRepository.listAll();
        for (Course c : courses) {
            if (c.getGroups().contains(group)) {
                return c.signUpStudentToCourse(group, studentRepository.getById(studentIndex));
            }
        }
        return Course.COURSE_ERROR.STUDENT_IN_COURSE;
//        group.addStudent(studentRepository.getById(studentIndex));
//        groupRepository.update(group);
    }

    public void addGroup(String courseCode, String groupCode, Integer capacity) {
        Course course = courseRepository.getById(courseCode);
        Course.Group group = course.addCourseGroup(groupCode, capacity);
        courseRepository.update(course);
    }

    public void addCourse(String courseName, String courseCode) {
        Course course = new Course(courseName, courseCode);
        courseRepository.add(course);
    }


    public void removeStudent(String studentIndex) {
        List<Course> courses = courseRepository.listAll();
        Student student = studentRepository.getById(studentIndex);
        for (Course c : courses) {
            c.removeStudentFromCourse(student);
        }
        studentRepository.remove(student);
    }

    public void removeGroup(String groupCode) {
        Course.Group g = groupRepository.getById(groupCode);
        List<Course> courses = courseRepository.listAll();
        for (Course c : courses) {
            if (c.getGroups().contains(g)) {
                for (Student s : g.getStudentsList()) {
                    c.removeStudentFromCourse(s);
                }
            }
        }
        groupRepository.remove(g);
    }

    public void removeCourse(String courseCode) {
        Course course = courseRepository.getById(courseCode);
        for (Course.Group g : course.getGroups()) {
            groupRepository.remove(g);
        }
        course.getGroups().clear();
        courseRepository.remove(course);
    }


    public List<Student> listAllStudents() {
        return studentRepository.listAll();
    }

    public List<Course> listAllCourses() {
        return courseRepository.listAll();
    }

    public Set<Course.Group> listGroupsInCourse(String courseCode) {
        return courseRepository.getById(courseCode).getGroups();
    }

    public Set<Student> listStudentsInCourseGroup(String groupCode) {
        return groupRepository.getById(groupCode).getStudentsList();
    }

    public void changeGroupCapacity(String groupCode, Integer capacity) {
        Course.Group group = groupRepository.getById(groupCode);
        group.setGroupCapacity(capacity);
        groupRepository.update(group);
    }

    public void unsubStudentCourse(String courseCode, String studentIndex) {
        Course course = courseRepository.getById(courseCode);
        Student student = studentRepository.getById(studentIndex);
        course.removeStudentFromCourse(student);
//        for(Course.Group cg : course.getGroups()){
//            if(cg.getStudentsList().contains(student)){
//                cg.getStudentsList().remove(student);
//                break;
//            }
//        }
        courseRepository.update(course);
    }

    public void closeConnectionToDatabase() {
        pm.close();
        pmf.close();
    }
}
