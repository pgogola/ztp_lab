package lab.zmp.zero.view;

import lab.zmp.zero.controller.IController;
import lab.zmp.zero.model.Course;
import lab.zmp.zero.model.Student;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class TestView implements IView {

    private String[] testCommands = {
            "addstud Jan Kowalski 123456",
            "addcourse Matematyka M123",
            "addcourse Fizyka F123",
            "addgroup M123 MG1 2",
            "addgroup M123 FG1 10",
            "addstud Marcin Kowalski 653219",
            "addstud Patryk Iksinski 123456",
            "addstud Patryk Iksinski 654321",
            "addstog 123456 MG1",
            "addstog 653219 MG1",
            "addstog 654321 MG1",
            "addstog 123456 FG1",
            "addstog 123456 KG1",
            "chgcap MG1 10",
            "addstog 654321 MG1",
            "remcourse F123",
            "lss",
            "exit"
    };

    private int i = 0;

    private IController controllerListener;
    private final static Scanner sc = new Scanner(System.in);

    public void setControllerListener(IController controllerListener) {
        this.controllerListener = controllerListener;
    }

    public boolean askUser() {
        System.err.print("~> ");
        System.err.println(testCommands[i]);
        return controllerListener.viewHandler(testCommands[i++]);
    }

    public void listCourses(List<Course> courses) {
        for (Course cm : courses) {
            System.out.println(cm.getName() + " " + cm.getCode() + " ");
        }
    }

    public void listGroupsInCourse(Set<Course.Group> groups) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Course.Group group = (Course.Group) it.next();
            System.out.println(group.getCode() + " " + group.getSignedUpStudentsAmount() + " " + group.getGroupCapacity());
        }
    }

    public void listStudents(List<Student> students) {
        for (Student sm : students) {
            System.out.println(sm.getName() + " " + sm.getSurname() + " " + sm.getIndex());
        }
    }

    public void listStudentsInCourseGroup(Set<Student> students) {
        Iterator it = students.iterator();
        while (it.hasNext()) {
            Student sm = (Student) it.next();
            System.out.println(sm.getName() + " " + sm.getSurname() + " " + sm.getIndex());
        }
    }

    public void showManual(List<String> manual) {
        for (String s : manual) {
            System.out.println(s);
        }
    }

    public void printMessage(String message) {
        if (null != message) {
            System.err.println(message);
        }
    }


}
