package lab.zmp.zero.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class Student {
    private final Integer INDEX_LENGTH = 6;

    public Student() {
    }

    public Student(String name, String surname, String index) {
        if (index.length() < INDEX_LENGTH) {
            throw new IllegalArgumentException("Student index has too short index length");
        }
        if (index.length() > INDEX_LENGTH) {
            throw new IllegalArgumentException("Student index has too long index length");
        }

        for (Character ch : index.toCharArray()) {
            Integer val = Character.getNumericValue(ch);
            if (val < 0 || val > 9){
                throw new IllegalArgumentException("Student index illegal format");
            }
        }
        this.name = name;
        this.surname = surname;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getIndex(){
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof Student && this.getIndex().equals(((Student) obj).getIndex()));
    }


    private String name;
    private String surname;

    @PrimaryKey
    private String index;
}

