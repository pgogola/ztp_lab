package lab.zmp.zero.other;

import java.util.*;

public class OptionParser {

    public class Option {
        final private String option;
        final private int requiredArgs;
        final private String description;
        private List<String> arguments;

        public Option(String option, int requiredArgs, String description) {
            this.option = option;
            this.requiredArgs = requiredArgs;
            this.description = description;
        }

        public Option(String option, int requiredArgs) {
            this(option, requiredArgs, "");
        }

        public String getOption() {
            return option;
        }

        public int getRequiredArgs() {
            return requiredArgs;
        }

        public String getDescription() {
            return description;
        }

        public List<String> getArguments() {
            return arguments;
        }

        private void setArguments(List<String> arguments) {
            this.arguments = arguments;
        }
    }

    private HashMap<String, Option> options;

    public List<String> listAvailableOptions() {
        List<String> opts = new ArrayList<>();
        for (Map.Entry<String, Option> stringOptionEntry : options.entrySet()) {
            Option option = (Option) ((Map.Entry) stringOptionEntry).getValue();
            opts.add(option.getOption() + " " + option.getDescription());
        }
        return opts;
    }

    public OptionParser() {
        options = new HashMap<>();
    }

    public boolean addOption(String option, int requiredArgs, String description) {
        if (options.containsKey(option)) {
            return false;
        }
        options.put(option, new Option(option, requiredArgs, description));
        return true;
    }

    public boolean addOption(String option, int requiredArgs) {
        return this.addOption(option, requiredArgs, "");
    }

    public Option parse(String command) {
        String[] splitted = command.split("\\s+");
        Option option = options.get(splitted[0]);
        if (null == option || splitted.length != option.getRequiredArgs() + 1) {
            return null;
        }
        option.setArguments(Arrays.asList(Arrays.copyOfRange(splitted, 1, option.getRequiredArgs() + 1)));
        return option;
    }
}