package lab.zmp.zero.controller;

import lab.zmp.zero.model.EnrolmentModel;
import lab.zmp.zero.other.OptionParser;
import lab.zmp.zero.view.IView;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOObjectNotFoundException;

public class Controller implements IController {

    private final String MESSAGE_ERROR = "Niepoprawna komenda lub niepoprawny format danych.\n" +
            "Wpisz 'help' aby uzyskac liste dostepnych instrukcji";

    private final IView view;
    private final EnrolmentModel enrolmentModel;

    private OptionParser parser = new OptionParser();

    public Controller(IView view, EnrolmentModel enrolmentModel) {
        this.view = view;
        this.enrolmentModel = enrolmentModel;
        view.setControllerListener(this);

        parser.addOption("addstud", 3, "<name> <surname> <index>"); //add student
        parser.addOption("addgroup", 3, "<courseCode> <groupCode> <capacity>"); //add group
        parser.addOption("addcourse", 2, "<courseName> <courseCode>"); //add course

        parser.addOption("remstud", 1, "<index>"); //remove student
        parser.addOption("remgroup", 1, "<groupCode>"); //remove group
        parser.addOption("remcourse", 1, "<courseCode>"); //remove course
        parser.addOption("remsfromg", 2, "<courseCode> <studentIndex>"); //remove student from course

        parser.addOption("addstog", 2, "<studentIndex> <groupCode>"); //add student to group

        parser.addOption("chgcap", 2, "<groupCode> <newCapacity>");     //list all students
        parser.addOption("lss", 0, "list all students");     //list all students
        parser.addOption("lsc", 0, "list courses");     //list courses
        parser.addOption("lsginc", 1, "<courseCode>");  //list groups in course
        parser.addOption("lssincg", 1, "<courseGroupCode>"); //list students in course group
        parser.addOption("help", 0);    //show help
        parser.addOption("exit", 0);    //exit program

    }

    @Override
    public boolean viewHandler(String command) {
        try {
            String message = null;
            OptionParser.Option option = parser.parse(command);
            if (null != option) {
                switch (option.getOption()) {
                    case "addstud":
                        enrolmentModel.addStudent(option.getArguments().get(0), option.getArguments().get(1), option.getArguments().get(2));
                        break;
                    case "addstog":
                        switch (enrolmentModel.addStudentToGroup(option.getArguments().get(0), option.getArguments().get(1))){
                            case OK:
                                message = "Student zostal pomyslnie zapisany na kurs";
                                break;
                            case FULL_GROUP:
                                message = "Grupa zajeciowa jest pelna. Student nie zostal zapisany na kurs";
                                break;
                            case STUDENT_IN_COURSE:
                                message = "Student jest juz zapisany na kurs";
                                break;
                        }
                        break;
                    case "addgroup":
                        enrolmentModel.addGroup(option.getArguments().get(0), option.getArguments().get(1), Integer.parseInt(option.getArguments().get(2)));
                        break;
                    case "addcourse":
                        enrolmentModel.addCourse(option.getArguments().get(0), option.getArguments().get(1));
                        break;

                    case "remstud":
                        enrolmentModel.removeStudent(option.getArguments().get(0));
                        break;
                    case "remgroup":
                        enrolmentModel.removeGroup(option.getArguments().get(0));
                        break;
                    case "remcourse":
                        enrolmentModel.removeCourse(option.getArguments().get(0));
                        break;
                    case "remsfromg":
                        enrolmentModel.unsubStudentCourse(option.getArguments().get(0), option.getArguments().get(1));
                        break;

                    case "chgcap":
                        enrolmentModel.changeGroupCapacity(option.getArguments().get(0), Integer.parseInt(option.getArguments().get(1)));
                        break;

                    case "lss":
                        view.listStudents(enrolmentModel.listAllStudents());
                        break;
                    case "lsc":
                        view.listCourses(enrolmentModel.listAllCourses());
                        break;
                    case "lsginc":
                        view.listGroupsInCourse(enrolmentModel.listGroupsInCourse(option.getArguments().get(0)));
                        break;
                    case "lssincg":
                        view.listStudentsInCourseGroup(enrolmentModel.listStudentsInCourseGroup(option.getArguments().get(0)));
                        break;
                    case "help":
                        view.showManual(parser.listAvailableOptions());
                        break;
                    case "exit":
                        enrolmentModel.closeConnectionToDatabase();
                        return false;
                }
                view.printMessage(message);
            } else {
                view.printMessage(MESSAGE_ERROR);
            }
        } catch (IllegalArgumentException e) {
            view.printMessage(e.getMessage());
        } catch (JDOObjectNotFoundException e) {
            view.printMessage("Obiekt nie znaleziony w bazie danych");
        }catch (JDODataStoreException e) {
            view.printMessage("Obiekt o podanym kluczu już istnieje w bazie");
        }
        return true;
    }

    public void run() {
        while (view.askUser());
    }
}
