grammar ClassGrammar;

/* create static abstract/final private/public/package/protected classname */
/* static abstract/final private/public/package/protected methodname */

classFileDef: classDeclaration+ EOF;

classDeclaration:
	NewClass classStaticModifier? classAbstractFinalModifier? classVisibilityModifier? className ':'
		classAttributes? classType? ';';

classType: arrow ClassType;

classStaticModifier: StaticModifier;
classAbstractFinalModifier: AbstractFinalModifier;
classVisibilityModifier: VisibilityModifier;
className: Identifier;

classAttributes: classAttribute (',' classAttribute)*;
classAttribute: methodDeclaration | attributeDeclaration;

methodDeclaration:
	methodStaticModifier? methodAbstractFinalModifier? methodVisibilityModifier? methodName '('
		argumentsList? ')' ':' methodReturnType;
methodStaticModifier: StaticModifier;
methodAbstractFinalModifier: AbstractFinalModifier;
methodVisibilityModifier: VisibilityModifier;
methodName: LowerCaseName;
argumentsList: argument (',' argument)*;
argument: argumentName ':' argumentType;
argumentName: LowerCaseName;
argumentType:
	Identifier
	| integralType
	| floatingPointType
	| booleanType;
methodReturnType:
	Identifier
	| integralType
	| floatingPointType
	| booleanType;

attributeDeclaration:
	attributeStaticModifier? attributeFinalModifier? attributeVisibilityModifier? attributeName ':'
		(
		attributeType
		| (attributeTypeWithValue)
	) attrProperty?;

attributeStaticModifier: StaticModifier;
attributeVisibilityModifier: VisibilityModifier;
attributeFinalModifier: FinalModifier;

attributeName: LowerCaseName;
attributeType:
	Identifier
	| integralType
	| floatingPointType
	| booleanType
	| stringType;

attributeTypeWithValue: (integralType '=' integerValue)
	| (floatingPointType '=' floatingPointValue)
	| (booleanType '=' ('true' | 'false'))
	| (stringType '=' stringValue);

attrProperty: (GETTER SETTER?) | (SETTER GETTER?);
integerValue: IntegerLiteral;
floatingPointValue: FloatingPointLiteral;
stringValue: StringLiteral;

ClassType: ( SINGLETON | BUILDER);
NewClass: CREATE;

AbstractFinalModifier: ABSTRACT | FINAL;
VisibilityModifier: PACKAGE | PRIVATE | PROTECTED | PUBLIC;
StaticModifier: STATIC;
FinalModifier: FINAL;
Identifier: BigLetter Character*;
LowerCaseName: SmallLetter Character*;
arrow: '->';
stringType: 'String';
booleanType: 'boolean';
integralType: 'byte' | 'short' | 'int' | 'long' | 'char';
floatingPointType: 'float' | 'double';

StringLiteral: '"' (~["\\\r\n] | EscapeSequence)* '"';
IntegerLiteral: Digit+;
FloatingPointLiteral: (Digit+ '.' Digit*) | ('.' Digit+);

fragment Digit: [0-9];
fragment BigLetter: [A-Z];
fragment SmallLetter: [a-z];
fragment Letter: [a-zA-Z];
fragment Sign: [+-];
fragment Underscores: '_'+;
fragment Character: [a-zA-Z_];
fragment EscapeSequence: '\\' [btnfr"'\\];

SINGLETON: 'singleton';
BUILDER: 'builder';
ABSTRACT: 'abstract';
CREATE: 'create';
PACKAGE: 'package';
PRIVATE: 'private';
PROTECTED: 'protected';
PUBLIC: 'public';
FINAL: 'final';
STATIC: 'static';
GETTER: '$get';
SETTER: '$set';
NULL: 'null';

WS: [ \t\r\n\u000C]+ -> skip;