package com.zmp.zad6;

import java.util.List;

import com.zmp.zad6.ClassGrammarParser.ArgumentContext;
import com.zmp.zad6.ClassGrammarParser.AttrPropertyContext;
import com.zmp.zad6.ClassGrammarParser.AttributeDeclarationContext;
import com.zmp.zad6.ClassGrammarParser.AttributeTypeContext;
import com.zmp.zad6.ClassGrammarParser.AttributeTypeWithValueContext;
import com.zmp.zad6.ClassGrammarParser.AttributeVisibilityModifierContext;
import com.zmp.zad6.ClassGrammarParser.ClassAbstractFinalModifierContext;
import com.zmp.zad6.ClassGrammarParser.ClassDeclarationContext;
import com.zmp.zad6.ClassGrammarParser.ClassNameContext;
import com.zmp.zad6.ClassGrammarParser.ClassTypeContext;
import com.zmp.zad6.ClassGrammarParser.ClassVisibilityModifierContext;
import com.zmp.zad6.ClassGrammarParser.MethodAbstractFinalModifierContext;
import com.zmp.zad6.ClassGrammarParser.MethodDeclarationContext;
import com.zmp.zad6.ClassGrammarParser.MethodVisibilityModifierContext;
import com.zmp.zad6.class_elements.Attribute;
import com.zmp.zad6.class_elements.ClassToGenerate;
import com.zmp.zad6.class_elements.Method;
import com.zmp.zad6.class_elements.ClassUtils.CLASS_TYPE;
import com.zmp.zad6.class_elements.ClassUtils.VISIBILITY;

public class ClassBuilderParser extends ClassGrammarBaseListener {

    List<ClassToGenerate> parsedClasses;

    ClassToGenerate classToGenerate = null;
    ClassToGenerate.Builder classBuilder = null;
    Method.Builder methodBuilder = null;
    Attribute.Builder attributeBuilder = null;

    public ClassBuilderParser(List<ClassToGenerate> classes) {
        this.parsedClasses = classes;
    }

    @Override
    public void enterClassDeclaration(ClassDeclarationContext ctx) {
        classBuilder = new ClassToGenerate.Builder();
    }

    @Override
    public void enterClassName(ClassNameContext ctx) {
        classBuilder.setClassName(ctx.getText());
    }

    @Override
    public void enterClassVisibilityModifier(ClassVisibilityModifierContext ctx) {
        switch (ctx.getText()) {
        case "private":
            classBuilder.setVisibility(VISIBILITY.PRIVATE);
            break;
        case "package":
            classBuilder.setVisibility(VISIBILITY.PACKAGE);
            break;
        case "public":
            classBuilder.setVisibility(VISIBILITY.PUBLIC);
            break;
        case "protected":
            classBuilder.setVisibility(VISIBILITY.PROTECTED);
            break;
        }
    }

    @Override
    public void enterClassType(ClassTypeContext ctx) {
        switch (ctx.ClassType().getText()) {
        case "singleton":
            classBuilder.setClassType(CLASS_TYPE.SINGLETON);
            break;
        case "builder":
            classBuilder.setClassType(CLASS_TYPE.BUILDER);
            break;
        }
    }

    @Override
    public void enterClassAbstractFinalModifier(ClassAbstractFinalModifierContext ctx) {
        switch (ctx.getText()) {
        case "abstract":
            classBuilder.setAbstract(true);
            break;
        case "final":
            classBuilder.setFinal(true);
            break;
        }
    }

    @Override
    public void exitClassDeclaration(ClassDeclarationContext ctx) {
        parsedClasses.add(classBuilder.build());
        classBuilder = null;
    }

    @Override
    public void enterMethodDeclaration(MethodDeclarationContext ctx) {
        methodBuilder = new Method.Builder();
        methodBuilder.setName(ctx.methodName().getText());
        methodBuilder.setReturnType(ctx.methodReturnType().getText());
    }

    @Override
    public void enterArgument(ArgumentContext ctx) {
        methodBuilder.addArgument(ctx.argumentName().getText(), ctx.argumentType().getText());
    }

    @Override
    public void enterMethodAbstractFinalModifier(MethodAbstractFinalModifierContext ctx) {
        switch (ctx.getText()) {
        case "abstract":
            methodBuilder.setAbstract(true);
            break;
        case "final":
            methodBuilder.setFinal(true);
            break;
        }
    }

    @Override
    public void enterMethodVisibilityModifier(MethodVisibilityModifierContext ctx) {
        switch (ctx.getText()) {
        case "private":
            methodBuilder.setVisibility(VISIBILITY.PRIVATE);
            break;
        case "package":
            methodBuilder.setVisibility(VISIBILITY.PACKAGE);
            break;
        case "public":
            methodBuilder.setVisibility(VISIBILITY.PUBLIC);
            break;
        case "protected":
            methodBuilder.setVisibility(VISIBILITY.PROTECTED);
            break;
        }
    }

    @Override
    public void exitMethodDeclaration(MethodDeclarationContext ctx) {
        classBuilder.addMethods(methodBuilder.build());
        methodBuilder = null;
    }

    @Override
    public void enterAttributeDeclaration(AttributeDeclarationContext ctx) {
        attributeBuilder = new Attribute.Builder();
        attributeBuilder.setName(ctx.attributeName().getText());
    }

    @Override
    public void enterAttributeVisibilityModifier(AttributeVisibilityModifierContext ctx) {
        switch (ctx.getText()) {
        case "private":
            attributeBuilder.setVisibility(VISIBILITY.PRIVATE);
            break;
        case "package":
            attributeBuilder.setVisibility(VISIBILITY.PACKAGE);
            break;
        case "public":
            attributeBuilder.setVisibility(VISIBILITY.PUBLIC);
            break;
        case "protected":
            attributeBuilder.setVisibility(VISIBILITY.PROTECTED);
            break;
        }
    }

    @Override
    public void enterAttributeType(AttributeTypeContext ctx) {
        attributeBuilder.setType(ctx.getText());
    }

    @Override
    public void enterAttrProperty(AttrPropertyContext ctx) {
        attributeBuilder.setHasGetter(null != ctx.GETTER());
        attributeBuilder.setHasSetter(null != ctx.SETTER());
    }

    @Override
    public void enterAttributeTypeWithValue(AttributeTypeWithValueContext ctx) {
        attributeBuilder.setType(ctx.getChild(0).getText());
        attributeBuilder.setInitialValue(ctx.getChild(2).getText());
    }

    @Override
    public void exitAttributeDeclaration(AttributeDeclarationContext ctx) {
        classBuilder.addAttribute(attributeBuilder.build());
        attributeBuilder = null;
    }
}