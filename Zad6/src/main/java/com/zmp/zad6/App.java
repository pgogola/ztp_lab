package com.zmp.zad6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.zmp.zad6.class_elements.ClassToGenerate;

import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class App {
    public static void main(String[] args) {
        Logger fileAccessLogger = Logger.getLogger("FileAccessLogger");
        Logger parserLogger = Logger.getLogger("ParserLogger");

        try {
            fileAccessLogger.addHandler(new FileHandler("FileAccessLogger.log", true));
            parserLogger.addHandler(new FileHandler("ParserLogger.log", true));
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
            fileAccessLogger.addHandler(new ConsoleHandler());
            parserLogger.addHandler(new ConsoleHandler());
        }

        LogManager logManager = LogManager.getLogManager();
        logManager.addLogger(parserLogger);
        logManager.addLogger(fileAccessLogger);

        String fileName = "./target/classes/com/zmp/zad6/class.txt";
        fileName = JOptionPane.showInputDialog("Podaj plik źródłowy", fileName);

        String fileContent = "";
        boolean dataReady = false;
        do {
            try (Scanner fileScanner = new Scanner(new File(fileName))) {
                fileContent = fileScanner.useDelimiter("\\Z").next();
                dataReady = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                fileAccessLogger.log(Level.WARNING, e.getMessage());
                fileName = JOptionPane.showInputDialog("Błąd - Podany plik nie znaleziony.\nPodaj plik źródłowy",
                        fileName);
            } catch (NullPointerException e) {
                e.printStackTrace();
                fileAccessLogger.log(Level.WARNING, e.getMessage());
                int n = JOptionPane.showConfirmDialog(null,
                        "Błąd - Podany plik nie znaleziony.\nCzy zakonczyc dzialanie?", "Zakonczyc?",
                        JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        } while (!dataReady);
        ClassGrammarLexer classGrammarLexer = new ClassGrammarLexer(CharStreams.fromString(fileContent));
        CommonTokenStream tokens = new CommonTokenStream(classGrammarLexer);
        ClassGrammarParser parser = new ClassGrammarParser(tokens);
        parser.addErrorListener(new ClassBuilderParserErrorListener()); // add errors listener
        ParseTreeWalker treeWalker = new ParseTreeWalker();
        ParseTree tree = parser.classFileDef();
        List<ClassToGenerate> classes = new ArrayList<>();
        ClassBuilderParser classBuilderParser = new ClassBuilderParser(classes);
        treeWalker.walk(classBuilderParser, tree);

        String outputFolder = "./generatedClasses/";
        outputFolder = JOptionPane.showInputDialog("Podaj katalog do którego zostaną zapisane klasy", outputFolder);
        for (ClassToGenerate ctg : classes) {
            ClassGenerator cg = new ClassGenerator();
            try {
                cg.generateClassText(ctg);
            } catch (NullPointerException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                parserLogger.log(Level.WARNING, e.getMessage());
                System.exit(0);
            }
            boolean correctOutput = false;
            do {
                try (FileWriter fw = new FileWriter(new File(outputFolder + cg.getClassName() + ".java"))) {
                    if (null == outputFolder) {
                        throw new NullPointerException("outputFolder is null");
                    }
                    fw.write(cg.getClassDefinition());
                    correctOutput = true;
                } catch (IOException e) {
                    e.printStackTrace();
                    fileAccessLogger.log(Level.WARNING, e.getMessage());
                    outputFolder = JOptionPane.showInputDialog(
                            "Błąd - Dane niepoprawne, nie mozna zapisac pliku.\nPodaj katalog wyjsciowy", outputFolder);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    fileAccessLogger.log(Level.WARNING, e.getMessage());
                    int n = JOptionPane.showConfirmDialog(null,
                            "Błąd - Podana sciezka nieprawidlowa.\nCzy zakonczyc dzialanie?", "Zakonczyc?",
                            JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                }
            } while (!correctOutput);
        }

        // for (ClassToGenerate c : classes) {
        // System.out.println(c.getClassName());
        // System.out.println(c.isAbstract());
        // System.out.println(c.isFinal());
        // System.out.println(c.isStatic());
        // System.out.println(c.getVisibility());
        // for (Method m : c.getMethods()) {
        // System.out.print(m.getName() + "(");
        // for (Variable v : m.getArguments()) {
        // System.out.print(v.getName() + ":" + v.getType() + " ");
        // }
        // System.out.println(") :" + m.getReturnType());
        // }
        // for (Attribute a : c.getAttributes()) {
        // System.out.println(a.getName() + ":" + a.getType()
        // + (null == a.getInitialValue() ? "" : " = " + a.getInitialValue()));
        // System.out.println(a.isHasGetter());
        // System.out.println(a.isHasSetter());
        // }
        // }

        System.out.println(tree.toStringTree(parser));

        // show AST in GUI
        JFrame frame = new JFrame("Antlr AST");
        JPanel panel = new JPanel();
        TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
        viewr.setScale(1.0);// scale a little
        panel.add(viewr);
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 1000);
        frame.setVisible(true);

    }
}
