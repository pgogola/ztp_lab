package com.zmp.zad6.class_elements;

import com.zmp.zad6.class_elements.ClassUtils.VISIBILITY;

public class Attribute {
    private Variable variable;
    private boolean hasSetter;
    private boolean hasGetter;
    private boolean isFinal;
    private boolean isStatic;
    private String initialValue;
    private VISIBILITY visibility;

    private Attribute(String type, String name, VISIBILITY visibility, boolean hasSetter, boolean hasGetter,
            boolean isFinal, boolean isStatic, String initialValue) {
        this.variable = new Variable(name, type);
        this.visibility = visibility;
        this.hasGetter = hasGetter;
        this.hasSetter = hasSetter;
        this.isFinal = isFinal;
        this.isStatic = isStatic;
        this.initialValue = initialValue;
    }

    public static class Builder {
        private String name;
        private String type;
        private boolean hasSetter;
        private boolean hasGetter;
        private boolean isFinal;
        private boolean isStatic;
        private String initialValue;
        private VISIBILITY visibility;

        public Builder() {
            hasSetter = false;
            hasGetter = false;
            isFinal = false;
            isStatic = false;
            initialValue = null;
            visibility = VISIBILITY.PACKAGE;
        }

        /**
         * @param name the name to set
         */
        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param type the type to set
         */
        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        /**
         * @param visibility the visibility to set
         */
        public Builder setVisibility(VISIBILITY visibility) {
            this.visibility = visibility;
            return this;
        }

        /**
         * @param isFinal the isFinal to set
         */
        public Builder setFinal(boolean isFinal) {
            this.isFinal = isFinal;
            return this;
        }

        /**
         * @param hasGetter the hasGetter to set
         */
        public Builder setHasGetter(boolean hasGetter) {
            this.hasGetter = hasGetter;
            return this;
        }

        /**
         * @param hasSetter the hasSetter to set
         */
        public Builder setHasSetter(boolean hasSetter) {
            this.hasSetter = hasSetter;
            return this;
        }

        /**
         * @param initialValue the initialValue to set
         */
        public Builder setInitialValue(String initialValue) {
            this.initialValue = initialValue;
            return this;
        }

        /**
         * @param isStatic the isStatic to set
         */
        public Builder setStatic(boolean isStatic) {
            this.isStatic = isStatic;
            return this;
        }

        public Attribute build() {
            return new Attribute(type, name, visibility, hasSetter, hasGetter, isFinal, isStatic, initialValue);
        }

    }

    // /**
    // * @return the type
    // */
    // public String getType() {
    // return variable.getType();
    // }

    // /**
    // * @return the name
    // */
    // public String getName() {
    // return name;
    // }

    /**
     * @return the variable
     */
    public Variable getVariable() {
        return variable;
    }

    /**
     * @return the variable
     */
    public String getName() {
        return variable.getName();
    }

    /**
     * @return the variable
     */
    public String getType() {
        return variable.getType();
    }

    /**
     * @return the hasSetter
     */
    public boolean isHasSetter() {
        return hasSetter;
    }

    /**
     * @return the hasGetter
     */
    public boolean isHasGetter() {
        return hasGetter;
    }

    /**
     * @return the isFinal
     */
    public boolean isFinal() {
        return isFinal;
    }

    /**
     * @return the isStatic
     */
    public boolean isStatic() {
        return isStatic;
    }

    /**
     * @return the initialValue
     */
    public String getInitialValue() {
        return initialValue;
    }

    /**
     * @return the visibility
     */
    public VISIBILITY getVisibility() {
        return visibility;
    }
}