package com.zmp.zad6.class_elements;

import java.util.ArrayList;
import java.util.List;

import com.zmp.zad6.class_elements.ClassUtils.CLASS_TYPE;
import com.zmp.zad6.class_elements.ClassUtils.VISIBILITY;

public class ClassToGenerate {
    private String className;
    private VISIBILITY visibility;
    private CLASS_TYPE classType;
    private boolean isFinal;
    private boolean isStatic;
    private boolean isAbstract;
    private List<Attribute> attributes;
    private List<Method> methods;

    public ClassToGenerate(String className, VISIBILITY visibility, boolean isFinal, boolean isStatic,
            boolean isAbstract, List<Attribute> attributes, List<Method> methods, CLASS_TYPE classType) {
        this.className = className;
        this.visibility = visibility;
        this.isFinal = isFinal;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        this.attributes = attributes;
        this.methods = methods;
        this.classType = classType;
    }

    public static class Builder {
        private String className;
        private VISIBILITY visibility;
        private CLASS_TYPE classType;
        private boolean isFinal;
        private boolean isStatic;
        private boolean isAbstract;
        private List<Attribute> attributes;
        private List<Method> methods;

        public Builder() {
            visibility = VISIBILITY.PACKAGE;
            classType = CLASS_TYPE.NORMAL;
            isFinal = false;
            isStatic = false;
            isAbstract = false;
            attributes = new ArrayList<>();
            methods = new ArrayList<>();
        }

        /**
         * @param isAbstract the isAbstract to set
         */
        public Builder setAbstract(boolean isAbstract) {
            this.isAbstract = isAbstract;
            return this;
        }

        /**
         * @param attributes the attributes to set
         */
        public Builder addAttribute(Attribute attribute) {
            this.attributes.add(attribute);
            return this;
        }

        /**
         * @param className the className to set
         */
        public Builder setClassName(String className) {
            this.className = className;
            return this;
        }

        /**
         * @param isFinal the isFinal to set
         */
        public Builder setFinal(boolean isFinal) {
            this.isFinal = isFinal;
            return this;
        }

        /**
         * @param isStatic the isStatic to set
         */
        public Builder setStatic(boolean isStatic) {
            this.isStatic = isStatic;
            return this;
        }

        /**
         * @param visibility the visibility to set
         */
        public Builder setVisibility(VISIBILITY visibility) {
            this.visibility = visibility;
            return this;
        }

        /**
         * @param methods the methods to set
         */
        public Builder addMethods(Method method) {
            this.methods.add(method);
            return this;
        }

        /**
         * @param classType the classType to set
         */
        public void setClassType(CLASS_TYPE classType) {
            this.classType = classType;
        }

        public ClassToGenerate build() {
            return new ClassToGenerate(className, visibility, isFinal, isStatic, isAbstract, attributes, methods,
                    classType);
        }
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @return the attributes
     */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * @return the visibility
     */
    public VISIBILITY getVisibility() {
        return visibility;
    }

    /**
     * @return the methods
     */
    public List<Method> getMethods() {
        return methods;
    }

    /**
     * @return the isAbstract
     */
    public boolean isAbstract() {
        return isAbstract;
    }

    /**
     * @return the isFinal
     */
    public boolean isFinal() {
        return isFinal;
    }

    /**
     * @return the isStatic
     */
    public boolean isStatic() {
        return isStatic;
    }

    /**
     * @return the classType
     */
    public CLASS_TYPE getClassType() {
        return classType;
    }
}