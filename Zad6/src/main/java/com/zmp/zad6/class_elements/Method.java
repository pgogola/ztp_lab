package com.zmp.zad6.class_elements;

import java.util.ArrayList;
import java.util.List;

import com.zmp.zad6.class_elements.ClassUtils.VISIBILITY;

public class Method {
    private String name;
    private String returnType;
    private List<Variable> arguments;
    private VISIBILITY visibility;
    private boolean isStatic;
    private boolean isAbstract;
    private boolean isFinal;

    private Method(String name, String returnType, VISIBILITY visibility, boolean isStatic, boolean isAbstract,
            boolean isFinal, List<Variable> arguments) {
        this.name = name;
        this.returnType = returnType;
        this.visibility = visibility;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        this.isFinal = isFinal;
        this.arguments = arguments;
    }

    public static class Builder {
        private String name;
        private String returnType;
        private List<Variable> arguments;
        private VISIBILITY visibility;
        private boolean isStatic;
        private boolean isAbstract;
        private boolean isFinal;

        public Builder() {
            isFinal = false;
            isAbstract = false;
            isStatic = false;
            visibility = VISIBILITY.PACKAGE;
            arguments = new ArrayList<>();
        }

        public Builder addArgument(Variable argument) {
            arguments.add(argument);
            return this;
        }

        public Builder addArgument(String name, String returnType) {
            addArgument(new Variable(name, returnType));
            return this;
        }

        /**
         * @param name the name to set
         */
        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param returnType the returnType to set
         */
        public Builder setReturnType(String returnType) {
            this.returnType = returnType;
            return this;
        }

        /**
         * @param isStatic the isStatic to set
         */
        public Builder setStatic(boolean isStatic) {
            this.isStatic = isStatic;
            return this;
        }

        /**
         * @param isAbstract the isAbstract to set
         */
        public Builder setAbstract(boolean isAbstract) {
            this.isAbstract = isAbstract;
            return this;
        }

        /**
         * @param isFinal the isFinal to set
         */
        public Builder setFinal(boolean isFinal) {
            this.isFinal = isFinal;
            return this;
        }

        /**
         * @param visibility the visibility to set
         */
        public Builder setVisibility(VISIBILITY visibility) {
            this.visibility = visibility;
            return this;
        }

        public Method build() {
            return new Method(name, returnType, visibility, isStatic, isAbstract, isFinal, arguments);
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the returnType
     */
    public String getReturnType() {
        return returnType;
    }

    /**
     * @return the arguments
     */
    public List<Variable> getArguments() {
        return arguments;
    }

    /**
     * @return the visibility
     */
    public VISIBILITY getVisibility() {
        return visibility;
    }

    /**
     * @return the isStatic
     */
    public boolean isStatic() {
        return isStatic;
    }

    /**
     * @return the isAbstract
     */
    public boolean isAbstract() {
        return isAbstract;
    }

    /**
     * @return the isFinal
     */
    public boolean isFinal() {
        return isFinal;
    }
}