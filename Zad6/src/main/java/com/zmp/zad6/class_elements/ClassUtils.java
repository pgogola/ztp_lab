package com.zmp.zad6.class_elements;

public abstract class ClassUtils {
    public static enum VISIBILITY {
        PRIVATE, PROTECTED, PUBLIC, PACKAGE
    }

    public static enum CLASS_TYPE {
        NORMAL, SINGLETON, BUILDER
    }
}