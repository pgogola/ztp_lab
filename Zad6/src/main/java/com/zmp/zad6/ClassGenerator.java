package com.zmp.zad6;

import com.zmp.zad6.class_elements.Attribute;
import com.zmp.zad6.class_elements.ClassToGenerate;
import com.zmp.zad6.class_elements.Method;
import com.zmp.zad6.class_elements.Variable;
import com.zmp.zad6.class_elements.ClassUtils.CLASS_TYPE;

public class ClassGenerator {
    private String className;
    private String classDefinition;

    public ClassGenerator() {
    }

    private String generateAttribute(Attribute attribute) {
        StringBuilder builder = new StringBuilder();
        if (attribute.isFinal())
            builder.append("final ");
        if (attribute.isStatic())
            builder.append("static ");

        switch (attribute.getVisibility()) {
        case PRIVATE:
            builder.append("private ");
            break;
        case PUBLIC:
            builder.append("public ");
            break;
        case PROTECTED:
            builder.append("protected ");
            break;
        default:
        }

        builder.append(attribute.getType()).append(" ").append(attribute.getName());
        if (null != attribute.getInitialValue()) {
            builder.append("=").append(attribute.getInitialValue());
        }
        builder.append(";");

        return builder.toString();
    }

    private String generateSetter(Attribute attribute) {
        return new StringBuilder().append("public ").append("void ").append("set")
                .append(attribute.getName().substring(0, 1).toUpperCase()).append(attribute.getName().substring(1))
                .append("(").append(attribute.getType()).append(" ").append(attribute.getName()).append("){\nthis.")
                .append(attribute.getName()).append("=").append(attribute.getName()).append(";\n}").toString();
    }

    private String generateGetter(Attribute attribute) {
        return new StringBuilder().append("public ").append(attribute.getType()).append(" get")
                .append(attribute.getName().substring(0, 1).toUpperCase()).append(attribute.getName().substring(1))
                .append("(){\nreturn this.").append(attribute.getName()).append(";\n}").toString();
    }

    private String generateMethod(Method method) {
        StringBuilder builder = new StringBuilder();
        if (method.isFinal())
            builder.append("final ");
        if (method.isStatic())
            builder.append("static ");
        if (method.isAbstract())
            builder.append("abstract ");

        switch (method.getVisibility()) {
        case PRIVATE:
            builder.append("private ");
            break;
        case PUBLIC:
            builder.append("public ");
            break;
        case PROTECTED:
            builder.append("protected ");
            break;
        default:
        }

        builder.append(method.getReturnType()).append(" ").append(method.getName()).append("(");
        for (Variable v : method.getArguments()) {
            if (v != method.getArguments().get(0)) {
                builder.append(", ");
            }
            builder.append(v.getType()).append(" ").append(v.getName());

        }
        builder.append(")");
        if (!method.isAbstract())
            builder.append("{\n}");
        else
            builder.append(";");

        return builder.toString();
    }

    private String generateInnerBuilderAndParentClassPrivateContructor(ClassToGenerate clazz) {
        StringBuilder builderClassStringBuilder = new StringBuilder();
        builderClassStringBuilder.append("public static class Builder{\n\n");

        for (Attribute a : clazz.getAttributes()) {
            if (!a.isStatic()) {
                builderClassStringBuilder.append("private ").append(a.getType()).append(" ").append(a.getName())
                        .append(";\n\n");
                builderClassStringBuilder.append("public Builder set").append(a.getName().substring(0, 1).toUpperCase())
                        .append(a.getName().substring(1)).append("(").append(a.getType()).append(" ")
                        .append(a.getName()).append("){\nthis.").append(a.getName()).append("=").append(a.getName())
                        .append(";\nreturn this;\n}\n\n");
            }
        }

        builderClassStringBuilder.append("public ").append(clazz.getClassName()).append(" build(){\n")
                .append("return new ").append(clazz.getClassName()).append("(");

        boolean getComma = false;
        for (Attribute a : clazz.getAttributes()) {
            if (!a.isStatic()) {
                if (getComma) {
                    builderClassStringBuilder.append(", ");
                }
                builderClassStringBuilder.append(a.getName());
                getComma = true;
            }
        }
        builderClassStringBuilder.append(");\n");

        builderClassStringBuilder.append("}\n\n}\n\n");
        builderClassStringBuilder.append("private ").append(clazz.getClassName()).append("(");
        getComma = false;
        for (Attribute a : clazz.getAttributes()) {
            if (!a.isStatic()) {
                if (getComma) {
                    builderClassStringBuilder.append(", ");
                }
                builderClassStringBuilder.append(a.getType()).append(" ").append(a.getName());
                getComma = true;
            }
        }
        builderClassStringBuilder.append("){");
        for (Attribute a : clazz.getAttributes()) {
            if (!a.isStatic()) {
                builderClassStringBuilder.append("this.").append(a.getName()).append(" = ").append(a.getName())
                        .append(";\n");
            }
        }
        builderClassStringBuilder.append("}\n\n");
        return builderClassStringBuilder.toString();

    }

    public void generateClassText(ClassToGenerate clazz) {
        if (clazz.getClassName() == null) {
            throw new NullPointerException("Błąd generatora.\nNie podano nazwy klasy.\nProgram zakończy działanie");
        }
        className = clazz.getClassName();
        StringBuilder builder = new StringBuilder();
        switch (clazz.getVisibility()) {
        case PRIVATE:
            builder.append("private ");
            break;
        case PUBLIC:
            builder.append("public ");
            break;
        case PROTECTED:
            builder.append("protected ");
            break;
        default:
        }

        if (clazz.isFinal())
            builder.append("final ");
        if (clazz.isAbstract())
            builder.append("abstract ");
        if (clazz.isStatic())
            builder.append("static ");
        builder.append("class ").append(clazz.getClassName()).append(" {\n\n");

        if (CLASS_TYPE.SINGLETON == clazz.getClassType()) {
            builder.append("private static final ").append(clazz.getClassName()).append(" singletonInstance = new ")
                    .append(clazz.getClassName()).append("();\n\n");
            builder.append("public static ").append(clazz.getClassName()).append(" getInstance(){\n")
                    .append("return singletonInstance;\n}").append("\n\n");

            builder.append("private ").append(clazz.getClassName()).append("(){\n").append("}").append("\n\n");
        } else if (CLASS_TYPE.BUILDER == clazz.getClassType()) {
            builder.append(generateInnerBuilderAndParentClassPrivateContructor(clazz));
        }

        for (Attribute a : clazz.getAttributes()) {
            builder.append(generateAttribute(a)).append("\n\n");
        }

        for (Method m : clazz.getMethods()) {
            builder.append(generateMethod(m)).append("\n\n");
        }

        for (Attribute a : clazz.getAttributes()) {
            if (a.isHasGetter())
                builder.append(generateGetter(a)).append("\n\n");
        }

        for (Attribute a : clazz.getAttributes()) {
            if (a.isHasSetter())
                builder.append(generateSetter(a)).append("\n\n");
        }

        builder.append("}");
        classDefinition = builder.toString();
    }

    /**
     * @return the classDefinition
     */
    public String getClassDefinition() {
        return classDefinition;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }
}