package com.zmp.zad6;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

public class ClassBuilderParserErrorListener extends BaseErrorListener {
    private Logger logger;

    private void logDate(String message) {
        logger.log(Level.WARNING, message);
    }

    public ClassBuilderParserErrorListener() {
        logger = LogManager.getLogManager().getLogger("ParserLogger");
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
            String msg, RecognitionException e) {
        logDate("Line: " + line + " position: " + charPositionInLine
                + (offendingSymbol.getClass().getName() == "org.antlr.v4.runtime.CommonToken"
                        ? (" error_token: " + ((CommonToken) offendingSymbol).getText())
                        : "")
                + " message " + msg);
    };

}