package com.zmp.zad3;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import com.zmp.zad3.SchoolClass;
import com.zmp.zad3.SchoolClass.ClassType;

public class CloneMethodTest {

    private Student student;
    private String firstName = "Jan";
    private String lastName = "Kowalski";
    private int age = 15;
    private int[] PESEL = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    private SchoolClass schoolClass;
    private int classLevel = 6;
    private char classNumber = 'c';
    private int classCapacity = 10;
    private SchoolClass.ClassType schoolClassType = ClassType.MATH;

    private void initSchoolClass() {
        String firstName1 = "Jan1";
        String lastName1 = "Kowalski1";
        int age1 = 1;
        int[] PESEL1 = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        Student s1 = new Student(firstName1, lastName1, age1, PESEL1);
        String firstName2 = "Jan2";
        String lastName2 = "Kowalski2";
        int age2 = 2;
        int[] PESEL2 = new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2 };
        Student s2 = new Student(firstName2, lastName2, age2, PESEL2);
        List<Student> students = new ArrayList<>(Arrays.asList(s1, s2));

        schoolClass.addStudents(students);
    }

    @Before
    public void setUp() {
        student = new Student(firstName, lastName, age, PESEL);
        schoolClass = new SchoolClass(classLevel, classNumber, classCapacity, schoolClassType);
        initSchoolClass();
    }

    @Test
    public void When_StudentsCloneMethodIsCalled_Expect_DeepCopy() {
        Student studentCopy = student.clone();
        assertEquals(student.getFirstname(), studentCopy.getFirstname());
        assertEquals(student.getLastname(), studentCopy.getLastname());
        assertEquals(student.getAge(), studentCopy.getAge());
        assertArrayEquals(student.getPESEL(), studentCopy.getPESEL());

        studentCopy.setFirstname("Marcin");
        studentCopy.setLastname("Nowak");
        studentCopy.setAge(10);
        studentCopy.getPESEL()[0] = 9;
        assertNotEquals(student.getFirstname(), studentCopy.getFirstname());
        assertNotEquals(student.getLastname(), studentCopy.getLastname());
        assertNotEquals(student.getAge(), studentCopy.getAge());
        assertNotEquals(PESEL, studentCopy.getPESEL());

        assertFalse(student.getFirstname() == studentCopy.getFirstname());
        assertFalse(student.getLastname() == studentCopy.getLastname());
        assertFalse(student.getAge() == studentCopy.getAge());
        assertFalse(student.getPESEL() == studentCopy.getPESEL());
    }

    @Test
    public void When_SchoolClassCloneMethodIsCalled_Expect_DeepCopy() {
        SchoolClass schoolClassCopy = schoolClass.clone();
        assertEquals(schoolClass.getClassNumber(), schoolClassCopy.getClassNumber());
        assertEquals(schoolClass.getClassLevel(), schoolClassCopy.getClassLevel());
        assertEquals(schoolClass.getClassCapacity(), schoolClassCopy.getClassCapacity());
        assertEquals(schoolClass.getClassType(), schoolClassCopy.getClassType());
        assertNotEquals(schoolClass.getStudents().get(0), schoolClassCopy.getStudents().get(0));

        schoolClassCopy.setClassLevel(10);
        assertNotEquals(schoolClassCopy.getClassLevel(), schoolClass.getClassLevel());

        assertEquals(schoolClass.getStudentByNumberInClass(0).getFirstname(),
                schoolClassCopy.getStudentByNumberInClass(0).getFirstname());
        schoolClass.getStudentByNumberInClass(0).setFirstname("XXXX");
        assertNotEquals(schoolClass.getStudentByNumberInClass(0).getFirstname(),
                schoolClassCopy.getStudentByNumberInClass(0).getFirstname());
    }

    @Test
    public void When_SchoolCloneMethodIsCalled_Expect_DeepCopy() {
        /*
         * To test
         */
    }

}