package com.zmp.zad3;

import java.util.Random;

import com.github.javafaker.Faker;

class FakerBuild {

    static private Faker faker = new Faker();
    static private Random random = new Random();

    static private int[] makeRandomPESEL() {
        return new int[] { random.nextInt(10), random.nextInt(10), random.nextInt(10), random.nextInt(10),
                random.nextInt(10), random.nextInt(10), random.nextInt(10), random.nextInt(10), random.nextInt(10) };
    }

    static public Student buildRandomStudent() {
        return new Student(faker.name().firstName(), faker.name().lastName(), random.nextInt(18), makeRandomPESEL());
    }

    static public SchoolClass buildRandomClassWithStudents(int studentsInClass) {
        SchoolClass schoolClass = new SchoolClass(random.nextInt(8) + 1, (char) (random.nextInt(26) + 'a'),
                studentsInClass, SchoolClass.ClassType.getRandomSchoolClassType());
        for (int i = 0; i < studentsInClass; i++) {
            schoolClass.addStudent(buildRandomStudent());
        }
        return schoolClass;
    }

    static public School buildRandomSchoolWithClassesAndStudents(int schoolClasses, int studentsInClass) {
        School school = new School(faker.address().cityName(), faker.address().streetName(), random.nextInt(100),
                faker.name().fullName(), School.SchoolType.getRandomSchoolType());
        for (int i = 0; i < schoolClasses; i++) {
            school.addSchoolClasse(buildRandomClassWithStudents(studentsInClass));
        }
        return school;
    }

}