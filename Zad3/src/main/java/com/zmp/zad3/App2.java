package com.zmp.zad3;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App2 {
    private static class Sizes {
        int schools;
        int classes;
        int students;

        Sizes(int schools, int classes, int students) {
            this.schools = schools;
            this.classes = classes;
            this.students = students;
        }
    }

    public static Sizes Sizes(int schools, int classes, int students) {
        return new Sizes(schools, classes, students);
    }

    public static void main(String[] args) {

        List<Sizes> sizes = List.of(Sizes(4, 10, 250), Sizes(4, 5, 500), Sizes(4, 50, 500), Sizes(8, 25, 500),
                Sizes(10, 250, 400), Sizes(10, 50, 2000));

        for (Sizes s : sizes) {
            System.out.println("\\textbf{Liczba szkół:} " + s.schools + "\\\\");
            System.out.println("\\textbf{Liczba klass w szkole:} " + s.classes + "\\\\");
            System.out.println("\\textbf{Liczba uczniów w klasie:} " + s.students + "\\\\");
            System.out.println("\\textbf{Liczba obiektów łącznie:} " + s.students * s.classes * s.schools + "");

            System.out.println(
                    "\\begin{table}[H]\n\\centering\n\\caption{Czasy wykonania głębokiej kopii obiektów w milisekundach dla: "
                            + s.schools + " szkół, " + s.classes + " klas, " + s.students + " uczniów. Razem: "
                            + s.students * s.classes * s.schools + " obiektów}\n\\begin{tabular}{l||c|c}\n");
            System.out.print("Iteracja & Metoda clone() & Konstruktor kopiujący \\\\ \n\\hline \n");
            int iterations = 10;
            long clone = 0;
            long constructor = 0;
            for (int i = 0; i < iterations; i++) {
                Tester tester = new Tester(s.schools, s.classes, s.students);
                tester.prepareTest();
                tester.testCloneMethod();
                tester.testCopyContructor();
                System.out.print((i + 1) + ". & ");
                System.out.print(tester.getCloneMethodElapsedTime() + " & ");
                System.out.print(tester.getCopyContructorElapsedTime() + " \\\\\n");
                clone += tester.getCloneMethodElapsedTime();
                constructor += tester.getCopyContructorElapsedTime();
            }
            System.out.println("\\hline\n\\hline\nŚrednia & " + ((float) clone) / iterations + " & "
                    + ((float) constructor) / iterations);

            System.out.println("\\end{tabular}\n\\end{table}");
            System.out.println("\\rule{16cm}{1pt}\n\n");
        }
    }
}
