package com.zmp.zad3;

public interface IDeepCopy<T> {
    T deepCopy();
}