package com.zmp.zad3;

interface IByteDeepCopy<T> {
    T byteDeepCopy();
}