package com.zmp.zad3;

public class Tester {
    private int schools;
    private int classesInSchool;
    private int studentsInClass;

    private long copyContructorElapsedTime;
    private long cloneMethodElapsedTime;
    private long frameworkCopyElapsedTime;
    private long byteCopyElapsedTime;

    private SchoolsOrganization schoolsOrganization;

    public Tester(int schools, int classesInSchool, int studentsInClass) {
        this.schools = schools;
        this.classesInSchool = classesInSchool;
        this.studentsInClass = studentsInClass;
        this.schoolsOrganization = new SchoolsOrganization();
    }

    public void prepareTest() {
        for (int i = 0; i < schools; i++) {
            schoolsOrganization
                    .addSchool(FakerBuild.buildRandomSchoolWithClassesAndStudents(classesInSchool, studentsInClass));
        }
    }

    public void testCopyContructor() {
        long startTime = System.currentTimeMillis();

        SchoolsOrganization schoolsOrganizationCopy = new SchoolsOrganization(this.schoolsOrganization);

        long stopTime = System.currentTimeMillis();
        this.copyContructorElapsedTime = stopTime - startTime;
    }

    public void testCloneMethod() {
        long startTime = System.currentTimeMillis();

        SchoolsOrganization schoolsOrganizationCopy = this.schoolsOrganization.clone();

        long stopTime = System.currentTimeMillis();
        this.cloneMethodElapsedTime = stopTime - startTime;
    }

    public void testFrameworkCopy() {
        long startTime = System.currentTimeMillis();

        SchoolsOrganization schoolsOrganizationCopy = this.schoolsOrganization.deepCopy();

        long stopTime = System.currentTimeMillis();
        this.frameworkCopyElapsedTime = stopTime - startTime;
    }

    public void testByteCopy() {
        long startTime = System.currentTimeMillis();

        SchoolsOrganization schoolsOrganizationCopy = this.schoolsOrganization.byteDeepCopy();

        long stopTime = System.currentTimeMillis();
        this.byteCopyElapsedTime = stopTime - startTime;
    }

    /**
     * @return the cloneMethodElapsedTime
     */
    public long getCloneMethodElapsedTime() {
        return cloneMethodElapsedTime;
    }

    /**
     * @return the copyContructorElapsedTime
     */
    public long getCopyContructorElapsedTime() {
        return copyContructorElapsedTime;
    }

    /**
     * @return the frameworkCopyElapsedTime
     */
    public long getFrameworkCopyElapsedTime() {
        return frameworkCopyElapsedTime;
    }

    /**
     * @return the byteCopyElapsedTime
     */
    public long getByteCopyElapsedTime() {
        return byteCopyElapsedTime;
    }
}