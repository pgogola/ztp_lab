package com.zmp.zad3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;

public class SchoolsOrganization
        implements Cloneable, Serializable, IDeepCopy<SchoolsOrganization>, IByteDeepCopy<SchoolsOrganization> {
    private static final long serialVersionUID = -3257977110596805220L;
    private List<School> schools;

    public SchoolsOrganization() {
        this.schools = new ArrayList<>();
    }

    public SchoolsOrganization(SchoolsOrganization other) {
        this.schools = new ArrayList<>(other.schools.size());
        for (School s : other.schools) {
            this.schools.add(new School(s));
        }
    }

    @Override
    public SchoolsOrganization clone() {
        try {
            SchoolsOrganization schoolsOrganization = (SchoolsOrganization) super.clone();
            schoolsOrganization.schools = new ArrayList<>(this.schools.size());
            for (School s : this.schools) {
                schoolsOrganization.schools.add(s.clone());
            }
            return schoolsOrganization;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public SchoolsOrganization deepCopy() {
        return SerializationUtils.clone(this);
    }

    /**
     * @return the schools
     */
    public List<School> getSchools() {
        return schools;
    }

    /**
     * @param schools the schools to set
     */
    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    public void addSchool(School school) {
        this.schools.add(school);
    }

    @Override
    public SchoolsOrganization byteDeepCopy() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStrm;
        try {
            outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(this);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            return (SchoolsOrganization) objInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}