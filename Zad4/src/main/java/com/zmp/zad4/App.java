package com.zmp.zad4;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    static private Scanner in = new Scanner(System.in);

    public static void main(String[] args) {

        HashMap<Integer, CarBuilder> carBuilders = new HashMap<>();
        int number = 1;
        String command;

        System.out.println("->");
        command = in.nextLine();
        String[] splitted = command.split("\\s+");
        while (splitted[0] != "exit") {

            if (splitted.length > 1) {
                Integer arg = Integer.parseInt(splitted[1]);
                CarBuilder cb = carBuilders.get(arg);
                if (splitted[0].equals("print")) {
                    cb.printState();
                } else if (splitted[0].equals("prev")) {
                    cb.prevProductionState();
                } else if (splitted[0].equals("next")) {
                    cb.nextProductionState();
                } else if (splitted[0].equals("exec")) {
                    cb.doProductionState();
                } else {

                }
            } else if (splitted[0].equals("list")) {
                for (Map.Entry<Integer, CarBuilder> entry : carBuilders.entrySet())
                    System.out.println("Key = " + entry.getKey() + ", State = " + entry.getValue());
            }

            if (splitted[0].equals("create")) {
                CarBuilder cBuilder = new CarBuilder(number);
                cBuilder.setProductionState(new InitProductionState());
                carBuilders.put(number, cBuilder);
                number++;
            }

            System.out.println("->");
            command = in.nextLine();
            splitted = command.split("\\s+");
        }
        System.out.println("Hello World!");

        CarBuilder carBuilder = new CarBuilder(1);
        carBuilder.setProductionState(new InitProductionState());
        while (carBuilder.getProductionState() != null) {
            carBuilder.getProductionState().printStateName();
            carBuilder.doProductionState();
            carBuilder.nextProductionState();
        }
    }
}
