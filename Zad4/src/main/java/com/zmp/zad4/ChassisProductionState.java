package com.zmp.zad4;

public class ChassisProductionState implements IProductionState {

    @Override
    public void nextState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new MountEngineState());
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new InitProductionState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        carBuilder.setLength(5.1f).setWheelSize(32.f).setWidth(3.5f);
    }

    @Override
    public void printStateName() {
        System.out.println("ChassisProductionState");
    }

    @Override
    public String toString() {
        return "ChassisProductionState";
    }
}