package com.zmp.zad4;

import java.util.Calendar;

public class InitProductionState implements IProductionState {

    @Override
    public void nextState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new ChassisProductionState());
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        carBuilder.setProducent("Volswagen").setModel("Golf").setProductionDate(Calendar.getInstance().getTime());
    }

    @Override
    public void printStateName() {
        System.out.println("InitProductionState");
    }

    @Override
    public String toString() {
        return "InitProductionState";
    }
}