package com.zmp.zad4;

import java.util.Calendar;

public class SellState implements IProductionState {

    private Car car = null;

    @Override
    public void nextState(CarBuilder carBuilder) {
        this.car = carBuilder.setSellDate(Calendar.getInstance().getTime()).setFinalPrice(100000).build();
        if (car != null) {
            carBuilder.setProductionState(new SoldState(car));
        }
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new FinishProductionState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        this.car = carBuilder.setSellDate(Calendar.getInstance().getTime()).setFinalPrice(100000).build();
    }

    @Override
    public void printStateName() {
        System.out.println("SellState");
    }

    @Override
    public String toString() {
        return "SellState";
    }
}