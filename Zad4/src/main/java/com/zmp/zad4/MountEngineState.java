package com.zmp.zad4;

import com.zmp.zad4.Car.FUEL_TYPE;

public class MountEngineState implements IProductionState {

    @Override
    public void nextState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new BodyProductionState());
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new ChassisProductionState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        carBuilder.setEngineCapacity(1.9f).setFuelType(FUEL_TYPE.PETROL);
    }

    @Override
    public void printStateName() {
        System.out.println("MountEngineState");
    }

    @Override
    public String toString() {
        return "MountEngineState";
    }
}