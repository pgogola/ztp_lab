package com.zmp.zad4;

import java.util.Calendar;

public class FinishProductionState implements IProductionState {

    @Override
    public void nextState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new SellState());
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new BodyProductionState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        carBuilder.setProductionDate(Calendar.getInstance().getTime());
    }

    @Override
    public void printStateName() {
        System.out.println("FinishProductionState");
    }

    @Override
    public String toString() {
        return "FinishProductionState";
    }
}