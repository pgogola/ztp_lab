package com.zmp.zad4;

import java.util.Date;

import com.zmp.zad4.Car.COLOR;
import com.zmp.zad4.Car.FUEL_TYPE;

public class CarBuilder {
    private int carBuilderID;

    private IProductionState productionState;

    private String model;
    private String producent;
    private Date productionDate;
    private FUEL_TYPE fuelType;
    private COLOR color;
    private int trunkPackage;
    private float engineCapacity;
    private float length;
    private float weight;
    private float width;
    private String VIN;
    private float wheelSize;
    private boolean hasSunroof;
    private int doorsAmount;
    private int seats;
    private Date sellDate;
    private int finalPrice;

    public CarBuilder(int carBuilderID) {
        this.carBuilderID = carBuilderID;
    }

    /**
     * @param productionState the productionState to set
     */
    public void setProductionState(IProductionState productionState) {
        this.productionState = productionState;
    }

    /**
     * @return the productionState
     */
    public IProductionState getProductionState() {
        return productionState;
    }

    public void nextProductionState() {
        if (productionState != null) {
            productionState.nextState(this);
        }
    }

    public void prevProductionState() {
        if (productionState != null) {
            productionState.previousState(this);
        }
    }

    public void doProductionState() {
        if (productionState != null) {
            productionState.doAction(this);
        }
    }

    public Car build() {
        return new Car(model, producent, productionDate, fuelType, color, trunkPackage, engineCapacity, length, weight,
                width, VIN, wheelSize, hasSunroof, doorsAmount, seats, sellDate, finalPrice);
    };

    /**
     * @param color the color to set
     */
    public CarBuilder setColor(COLOR color) {
        this.color = color;
        return this;
    }

    /**
     * @param doorsAmount the doorsAmount to set
     */
    public CarBuilder setDoorsAmount(int doorsAmount) {
        this.doorsAmount = doorsAmount;
        return this;
    }

    /**
     * @param engineCapacity the engineCapacity to set
     */
    public CarBuilder setEngineCapacity(float engineCapacity) {
        this.engineCapacity = engineCapacity;
        return this;
    }

    /**
     * @param fuelType the fuelType to set
     */
    public CarBuilder setFuelType(FUEL_TYPE fuelType) {
        this.fuelType = fuelType;
        return this;
    }

    /**
     * @param hasSunroof the hasSunroof to set
     */
    public CarBuilder setHasSunroof(boolean hasSunroof) {
        this.hasSunroof = hasSunroof;
        return this;
    }

    /**
     * @param length the length to set
     */
    public CarBuilder setLength(float length) {
        this.length = length;
        return this;
    }

    /**
     * @param model the model to set
     */
    public CarBuilder setModel(String model) {
        this.model = model;
        return this;
    }

    /**
     * @param producent the producent to set
     */
    public CarBuilder setProducent(String producent) {
        this.producent = producent;
        return this;
    }

    /**
     * @param productionDate the productionDate to set
     */
    public CarBuilder setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
        return this;
    }

    /**
     * @param seats the seats to set
     */
    public CarBuilder setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    /**
     * @param trunkPackage the trunkPackage to set
     */
    public CarBuilder setTrunkPackage(int trunkPackage) {
        this.trunkPackage = trunkPackage;
        return this;
    }

    /**
     * @param vIN the vIN to set
     */
    public CarBuilder setVIN(String VIN) {
        this.VIN = VIN;
        return this;
    }

    /**
     * @param weight the weight to set
     */
    public CarBuilder setWeight(float weight) {
        this.weight = weight;
        return this;
    }

    /**
     * @param wheelSize the wheelSize to set
     */
    public CarBuilder setWheelSize(float wheelSize) {
        this.wheelSize = wheelSize;
        return this;
    }

    /**
     * @param width the width to set
     */
    public CarBuilder setWidth(float width) {
        this.width = width;
        return this;
    }

    /**
     * @param sellDate the sellDate to set
     */
    public CarBuilder setSellDate(Date sellDate) {
        this.sellDate = sellDate;
        return this;
    }

    /**
     * @param the finalPrice to set
     */
    public CarBuilder setFinalPrice(int finalPrace) {
        this.finalPrice = finalPrace;
        return this;
    }

    /**
     * @return the carBuilderID
     */
    public int getCarBuilderID() {
        return carBuilderID;
    }

    public void printState() {
        this.productionState.printStateName();
    }

    @Override
    public String toString() {
        return productionState.toString();
    }
}