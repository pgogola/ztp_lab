package com.zmp.zad4;

interface IProductionState {
    void nextState(CarBuilder carBuilder);

    void previousState(CarBuilder carBuilder);

    void doAction(CarBuilder carBuilder);

    void printStateName();
}