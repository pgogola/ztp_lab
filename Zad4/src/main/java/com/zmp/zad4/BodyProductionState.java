package com.zmp.zad4;

import com.zmp.zad4.Car.COLOR;

public class BodyProductionState implements IProductionState {

    @Override
    public void nextState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new FinishProductionState());
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new MountEngineState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        carBuilder.setColor(COLOR.BLUE).setDoorsAmount(4).setHasSunroof(true).setSeats(5).setDoorsAmount(5)
                .setVIN("" + carBuilder.getCarBuilderID()).setTrunkPackage(100);
    }

    @Override
    public void printStateName() {
        System.out.println("BodyProductionState");
    }

    @Override
    public String toString() {
        return "BodyProductionState";
    }

}