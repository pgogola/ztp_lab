package com.zmp.zad4;

public class SoldState implements IProductionState {

    private Car car;

    public SoldState(Car car) {
        this.car = car;
    }

    @Override
    public void nextState(CarBuilder carBuilder) {
    }

    @Override
    public void previousState(CarBuilder carBuilder) {
        carBuilder.setProductionState(new SellState());
    }

    @Override
    public void doAction(CarBuilder carBuilder) {
        System.out.println(car);
    }

    @Override
    public void printStateName() {
        System.out.println("SoldState");
    }

    @Override
    public String toString() {
        return "SoldState";
    }
}