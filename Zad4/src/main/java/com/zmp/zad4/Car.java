package com.zmp.zad4;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Car {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    static enum FUEL_TYPE {
        ON, GASE, PETROL
    };

    static enum COLOR {
        RED, BLUE, GREEN, BLACK, WHITE, SILVER, ORANGE, YELLOW, PURPLE
    };

    private final String model;
    private final String producent;
    private final Date productionDate;
    private FUEL_TYPE fuelType;
    private COLOR color;
    private int trunkPackage;
    private float engineCapacity;
    private float length;
    private float weight;
    private float width;
    private final String VIN;
    private float wheelSize;
    private boolean hasSunroof;
    private int doorsAmount;
    private int seats;
    private final Date sellDate;
    private int finalPrice;

    public Car(String model, String producent, Date productionDate, FUEL_TYPE fuelType, COLOR color, int trunkPackage,
            float engineCapacity, float length, float weight, float width, String VIN, float wheelSize,
            boolean hasSunroof, int doorsAmount, int seats, Date sellDate, int finalPrice) {
        this.model = model;
        this.producent = producent;
        this.productionDate = productionDate;
        this.fuelType = fuelType;
        this.color = color;
        this.trunkPackage = trunkPackage;
        this.engineCapacity = engineCapacity;
        this.length = length;
        this.weight = weight;
        this.width = width;
        this.VIN = VIN;
        this.wheelSize = wheelSize;
        this.hasSunroof = hasSunroof;
        this.doorsAmount = doorsAmount;
        this.seats = seats;
        this.sellDate = sellDate;
        this.finalPrice = finalPrice;
    }

    /**
     * @return the color
     */
    public COLOR getColor() {
        return color;
    }

    /**
     * @return the doorsAmount
     */
    public int getDoorsAmount() {
        return doorsAmount;
    }

    /**
     * @return the engineCapacity
     */
    public float getEngineCapacity() {
        return engineCapacity;
    }

    /**
     * @return the fuelType
     */
    public FUEL_TYPE getFuelType() {
        return fuelType;
    }

    /**
     * @return the length
     */
    public float getLength() {
        return length;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @return the producent
     */
    public String getProducent() {
        return producent;
    }

    /**
     * @return the productionDate
     */
    public Date getProductionDate() {
        return productionDate;
    }

    /**
     * @return the seats
     */
    public int getSeats() {
        return seats;
    }

    /**
     * @return the trunkPackage
     */
    public int getTrunkPackage() {
        return trunkPackage;
    }

    /**
     * @return the vIN
     */
    public String getVIN() {
        return VIN;
    }

    /**
     * @return the weight
     */
    public float getWeight() {
        return weight;
    }

    /**
     * @return the wheelSize
     */
    public float getWheelSize() {
        return wheelSize;
    }

    /**
     * @return the width
     */
    public float getWidth() {
        return width;
    }

    /**
     * @param color the color to set
     */
    public void setColor(COLOR color) {
        this.color = color;
    }

    /**
     * @param doorsAmount the doorsAmount to set
     */
    public void setDoorsAmount(int doorsAmount) {
        this.doorsAmount = doorsAmount;
    }

    /**
     * @param engineCapacity the engineCapacity to set
     */
    public void setEngineCapacity(float engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    /**
     * @param fuelType the fuelType to set
     */
    public void setFuelType(FUEL_TYPE fuelType) {
        this.fuelType = fuelType;
    }

    /**
     * @param hasSunroof the hasSunroof to set
     */
    public void setHasSunroof(boolean hasSunroof) {
        this.hasSunroof = hasSunroof;
    }

    /**
     * @param length the length to set
     */
    public void setLength(float length) {
        this.length = length;
    }

    /**
     * @param seats the seats to set
     */
    public void setSeats(int seats) {
        this.seats = seats;
    }

    /**
     * @param trunkPackage the trunkPackage to set
     */
    public void setTrunkPackage(int trunkPackage) {
        this.trunkPackage = trunkPackage;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(float weight) {
        this.weight = weight;
    }

    /**
     * @param wheelSize the wheelSize to set
     */
    public void setWheelSize(float wheelSize) {
        this.wheelSize = wheelSize;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(float width) {
        this.width = width;
    }

    /**
     * @return the hasSunroof
     */
    public boolean isHasSunroof() {
        return hasSunroof;
    }

    /**
     * @return the finalPrice
     */
    public int getFinalPrice() {
        return finalPrice;
    }

    /**
     * @return the sellDate
     */
    public Date getSellDate() {
        return sellDate;
    }

    @Override
    public String toString() {
        return "Producent = " + this.producent + "\nModel = " + this.model + "\nVIM = " + this.VIN + "\nFinal price = "
                + this.finalPrice + "\nProduction date = "
                + (this.productionDate == null ? "BRAK" : dateFormat.format(this.productionDate)) + "\nFuel type = "
                + this.fuelType + "\nColor = " + this.color + "\nTrunk package = " + this.trunkPackage
                + "\nEngine capacity = " + this.engineCapacity + "\nLength = " + this.length + "\nWeight = "
                + this.weight + "\nWidth = " + this.width + "\nWheel size = " + this.wheelSize + "\nHas sunroof = "
                + (this.hasSunroof ? "Yes" : "No") + "\nDoors amount = " + this.doorsAmount + "\nSeats = " + this.seats
                + "\nSell date = " + (this.sellDate == null ? "BRAK" : dateFormat.format(this.sellDate))
                + "\nFinal price = " + this.finalPrice;
    }
}