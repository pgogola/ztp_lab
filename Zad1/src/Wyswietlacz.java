import lab.*;
import other.OptionParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Wyswietlacz {


    static class Kontener<T> {
        List<? extends T> bankAccounts = new ArrayList<>();

        public Kontener() {
        }

    }

    public void pokazDaneBankAccount(Kontener<? extends BankAccount> bankAccounts) {
        for (BankAccount ba : bankAccounts.bankAccounts) {
            System.out.println(ba.getType() + " " + ba.getBankAccountNum() + " " + ba.getQuota());
        }
    }

    public void pokazDanePrivateAccount(Kontener<? extends PrivateAccount> bankAccounts) {
        for (PrivateAccount ba : bankAccounts.bankAccounts) {
            System.out.println(ba.getType() + " " + ba.getBankAccountNum() + " " + ba.getQuota() + " " + ba.getName());
        }
    }

    public void pokazDaneYouthAccount(Kontener<? extends YouthAccount> bankAccounts) {
        for (YouthAccount ba : bankAccounts.bankAccounts) {
            System.out.println(ba.getType() + " " + ba.getBankAccountNum() + " " + ba.getQuota() + " " + ba.getName() + " " + ba.getAge());
        }
    }


    public static void main(String[] args) {

        Wyswietlacz wyswietlacz = new Wyswietlacz();
        Kontener<BankAccount> bankAccounts = new Kontener<>();
        bankAccounts.bankAccounts = List.of(new BankAccount(), new BankAccount(), new PrivateAccount("Name 2"), new YouthAccount("Name 3", 10));
        Kontener<PrivateAccount> privateAccounts = new Kontener<>();
        privateAccounts.bankAccounts = List.of(new PrivateAccount("Name 1"), new YouthAccount("Name 3", 10), new PrivateAccount("AAAA"), new YouthAccount("Name 30", 20));
        Kontener<YouthAccount> youthAccounts = new Kontener<>();
        youthAccounts.bankAccounts = List.of(new YouthAccount("Name 23", 10), new YouthAccount("Name 2", 10),
                new YouthAccount("Name 123", 4), new YouthAccount("Name 43", 1), new YouthAccount("Name 3", 3));

        Scanner in = new Scanner(System.in);

        OptionParser parser = new OptionParser();

        parser.addOption("x", 1, "<container>");
        parser.addOption("y", 1, "<container>");
        parser.addOption("z", 1, "<container>");

        parser.addOption("help", 0);    //Sshow help
        parser.addOption("exit", 0);    //exit program


        boolean isExit = false;

        while (!isExit) {
            System.out.print("~> ");
            try {
                OptionParser.Option option = parser.parse(in.nextLine());
                if (null != option) {
                    switch (option.getOption()) {
                        case "x":
                            switch (Integer.parseInt(option.getArguments().get(0))) {
                                case 1:
                                    wyswietlacz.pokazDaneBankAccount(bankAccounts);
                                    break;
                                case 2:
                                    wyswietlacz.pokazDaneBankAccount(privateAccounts);
                                    break;
                                case 3:
                                    wyswietlacz.pokazDaneBankAccount(youthAccounts);
                                    break;
                            }
                            break;
                        case "y":
                            switch (Integer.parseInt(option.getArguments().get(0))) {
                                case 1:
//                                    wyswietlacz.pokazDanePrivateAccount(bankAccounts);
                                    break;
                                case 2:
                                    wyswietlacz.pokazDanePrivateAccount(privateAccounts);
                                    break;
                                case 3:
                                    wyswietlacz.pokazDanePrivateAccount(youthAccounts);
                                    break;
                            }
                            break;
                        case "z":
                            switch (Integer.parseInt(option.getArguments().get(0))) {
                                case 1:
//                                    wyswietlacz.pokazDaneYouthAccount(bankAccounts);
                                    break;
                                case 2:
//                                    wyswietlacz.pokazDaneYouthAccount(privateAccounts);
                                    break;
                                case 3:
                                    wyswietlacz.pokazDaneYouthAccount(youthAccounts);
                                    break;
                            }
                            break;
                        case "help":
                            for (String s : parser.listAvailableOptions()) {
                                System.out.println(s);
                            }
                        case "exit":
                            isExit = true;
                    }
                } else {
                    System.out.println("Komenda nie znana");
                }
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }


    }

}
