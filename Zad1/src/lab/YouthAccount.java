package lab;

public class YouthAccount extends PrivateAccount {

    private final float PERCENTAGE_FEES = 0f;

    private Integer age;

    public YouthAccount(String name, Integer age) {
        super(name);
        this.age = age;
    }

    @Override
    public float chargeFees() {
        return this.chargeFees(PERCENTAGE_FEES);
    }

    @Override
    public String toString() {
        return "Konto bankowe dla młodych, nr. " + getBankAccountNum() + ", stan konta: " + getQuota();
    }

    public Integer getAge() {
        return age;
    }

    public String getType(){
        return "YouthAccount";
    }
}
