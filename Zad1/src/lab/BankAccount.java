package lab;

public class BankAccount {
    private static int lastBankAccountNum = 1;
    private final int bankAccountNum;
    private float quota;

    public BankAccount() {
        this.bankAccountNum = lastBankAccountNum++;
        this.quota = 0;
    }

    public int getBankAccountNum() {
        return bankAccountNum;
    }

    public float getQuota() {
        return quota;
    }

    public void setQuota(float quota) {
        this.quota = quota;
    }

    public float chargeFees(){
        return chargeFees(0.5f);
    }

    protected float chargeFees(float percentage){
        float fees =  percentage*getQuota();
        setQuota(getQuota()-fees);
        return fees;
    }

    @Override
    public String toString() {
        return "Zwykle konto bankowe, nr. " + bankAccountNum + ", stan konta: " + quota;
    }

    public String getType(){
        return "BankAccount";
    }
}
