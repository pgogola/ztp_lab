package lab;

public class CompanyAccount extends BankAccount {

    private final float PERCENTAGE_FEES = 0.25f;
    private float credit;

    public CompanyAccount() {
        super();
        this.credit = 0.0f;
    }

    @Override
    public float chargeFees() {
        return this.chargeFees(PERCENTAGE_FEES);
    }

    @Override
    public String toString() {
        return "Firmowe konto bankowe, nr. " + getBankAccountNum() + ", stan konta: " + getQuota();
    }

    public void setCredit(float credit){
        this.credit = credit;
    }

    public float getCredit() {
        return credit;
    }
}
