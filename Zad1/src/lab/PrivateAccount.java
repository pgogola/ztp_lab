package lab;

public class PrivateAccount extends BankAccount {

    private final float PERCENTAGE_FEES = 0.2f;

    private String name;

    public PrivateAccount(String name) {
        super();
        this.name = name;
    }

    @Override
    public float chargeFees() {
        return this.chargeFees(PERCENTAGE_FEES);
    }

    @Override
    public String toString() {
        return "Prywatne konto bankowe, nr. " + getBankAccountNum() + ", stan konta: " + getQuota();
    }

    public String getName() {
        return name;
    }

    public String getType(){
        return "PrivateAccount";
    }
}
