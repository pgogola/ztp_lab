package lab;

public class GoldCompanyAccount extends CompanyAccount {

    private final float PERCENTAGE_FEES = 0.25f;

    public GoldCompanyAccount() {
        super();
    }

    @Override
    public float chargeFees() {
        return this.chargeFees(PERCENTAGE_FEES);
    }

    @Override
    public String toString() {
        return "Firmowe konto bankowe, nr. " + getBankAccountNum() + ", stan konta: " + getQuota();
    }

}
