import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;


class CloneableClass implements Cloneable {
    @Override
    public CloneableClass clone() throws CloneNotSupportedException {
        return (CloneableClass) super.clone();
    }
}

public class Pair<T> implements Cloneable {
    private T obj1;
    private T obj2;
    private Class<T> clazz;

    public Pair(T e1, T e2) {
        this.obj1 = e1;
        this.obj2 = e2;
    }

    public Pair() {
        obj1 = null;
        obj2 = null;
    }

    public Pair(Class<T> clazz) {
        this.clazz = clazz;
        obj1 = null;
        obj2 = null;
    }

    public T getObj1() {
        return obj1;
    }

    public void setObj1(T obj1) {
        this.obj1 = obj1;
    }

    public T getObj2() {
        return obj2;
    }

    public void setObj2(T obj2) {
        this.obj2 = obj2;
    }

    public Class<T> getclass() {
        return clazz;
    }

    @Override
    protected Pair<T> clone() throws CloneNotSupportedException {
        Pair<T> clonedPair = null;
        try {
            clonedPair = (Pair<T>) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new CloneNotSupportedException(e.toString());
        }
        try {
            Class<?> clazz = this.obj1.getClass();
            Method cloneMethod = clazz.getMethod("clone");
            clonedPair.obj1 = (T) cloneMethod.invoke(this.obj1, new Object[0]);
        } catch (NoSuchMethodException e) {
            clonedPair.obj1 = this.obj1;
        } catch (Exception e) {
            throw new InternalError(e.toString());
        }
        try {
            Class<?> clazz = this.obj2.getClass();
            Method cloneMethod = clazz.getMethod("clone");
            clonedPair.obj2 = (T) cloneMethod.invoke(this.obj2, new Object[0]);
        } catch (NoSuchMethodException e) {
            clonedPair.obj2 = this.obj2;
        } catch (Exception e) {
            throw new InternalError(e.toString());
        }
        return clonedPair;
    }

    static public <T> Pair<T> createObject(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return (Pair<T>) Pair.class.getConstructor(Class.class).newInstance(clazz);
    }

    static public <T> Pair<T>[] createObject(Class<T> clazz, int arraySize) {
        return (Pair<T>[]) java.lang.reflect.Array.newInstance(Pair.class, arraySize);
    }

    static public void main(String[] args) {

        System.out.println("Typ T bez funkcji clone - Integer");
        Pair<Integer> notCloneable = new Pair<>(10, 15);
        Pair<Integer> notCloneableClone = null;
        try {
            notCloneableClone = notCloneable.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println(notCloneable.getObj1() == notCloneableClone.getObj1());

        System.out.println("Typ T z funkcją clone - CloneableClass");
        Pair<CloneableClass> cloneable = new Pair<>(new CloneableClass(), new CloneableClass());
        Pair<CloneableClass> cloneableClone = null;
        try {
            cloneableClone = cloneable.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println(cloneable.getObj1() == cloneableClone.getObj1());

        Scanner in = new Scanner(System.in);

        System.out.println("1. Utwórz tablice <typ> <wielkosc>");
        System.out.println("2. Utwórz obiekt");
        String input = null;
        input = in.nextLine();
        while (!"exit".equals(input)) {
            String[] arr = input.split("\\s+");
            switch (arr[0]) {
                case "1":
                    try {
                        Pair<?>[] pairs = createObject(Class.forName(arr[1]), Integer.parseInt(arr[2]));
                        System.out.println("Utworzono :");
                        System.out.println(pairs.getClass());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
                case "2":
                    try {
                        Pair<?> pair = createObject(Class.forName(arr[1]));
                        System.out.println("Utworzono :");
                        System.out.println(pair.getClass());
                        System.out.println(pair.getclass().getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

            System.out.println("1. Utwórz tablice par <typ> <wielkosc>");
            System.out.println("2. Utwórz obiekt <typ>");
            System.out.println("exit");
            input = in.nextLine();
        }


    }
}
